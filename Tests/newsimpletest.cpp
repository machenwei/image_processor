/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   newsimpletest.cpp
 * Author: machenwei
 *
 * Created on February 28, 2018, 6:05 PM
 */
#define IS_UNIT_TEST

#include <cassert>
#include <stdlib.h>
#include <iostream>

/*
 * Simple C++ Test Suite
 */

void test1() {
  //std::cout << "newsimpletest test 1" << std::endl;
  assert(1==1);
}

void test2() {
  //std::cout << "newsimpletest test 2" << std::endl;
  //std::cout << "%TEST_FAILED% time=0 testname=test2 (newsimpletest) message=error message sample" << std::endl;
}

int main(int argc, char** argv) {
  test1();
  test2();
  std::cout << "%SUITE_FINISHED% time=0" << std::endl;
  //return (EXIT_FAILURE);
  return (EXIT_SUCCESS);
}

