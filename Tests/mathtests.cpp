/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#define IS_UNIT_TEST

#include <cassert>
#include <stdlib.h>
#include <iostream>

#include "Common.h"
#include "Debug.h"

#include "ImageTransformMathFunctions.h"

using namespace std;

void test_NormalToEquirectangular() {
  float input_x, input_y;
  tie(input_x, input_y) = NormalToEquirectangular(960,
                                                  960,
                                                  1920,
                                                  1920,
                                                  960,
                                                  960,
                                                  0,
                                                  0,
                                                  90,
                                                  90);
  assert(input_x == 480 && input_y == 480);
}

void test_NormalToEquirectangularSymmetry() {
  float input_x, input_y;
  tie(input_x, input_y) = NormalToEquirectangular(900,
                                                  960,
                                                  1920,
                                                  1920,
                                                  960,
                                                  960,
                                                  0,
                                                  0,
                                                  90,
                                                  90);
  assert(input_x == 480 && int(input_y) == 546);
  tie(input_x, input_y) = NormalToEquirectangular(1020,
                                                  960,
                                                  1920,
                                                  1920,
                                                  960,
                                                  960,
                                                  0,
                                                  0,
                                                  90,
                                                  90);
  assert(input_x == 480 && int(input_y) == 413);
  
  tie(input_x, input_y) = NormalToEquirectangular(960,
                                                  900,
                                                  1920,
                                                  1920,
                                                  960,
                                                  960,
                                                  0,
                                                  0,
                                                  90,
                                                  90);
  assert(int(input_x) == 413 && input_y == 480);
  
  tie(input_x, input_y) = NormalToEquirectangular(960,
                                                  1020,
                                                  1920,
                                                  1920,
                                                  960,
                                                  960,
                                                  0,
                                                  0,
                                                  90,
                                                  90);
  assert(int(input_x) == 546 && input_y == 480);
}

void test_NormalToEquirectangularRectangularInput() {
  float input_x, input_y;
  tie(input_x, input_y) = NormalToEquirectangular(900,
                                                  960,
                                                  1920,
                                                  1920,
                                                  1920,
                                                  960,
                                                  0,
                                                  0,
                                                  90,
                                                  90);
  assert(input_x == 960 && int(input_y) == 546);
  tie(input_x, input_y) = NormalToEquirectangular(1020,
                                                  960,
                                                  1920,
                                                  1920,
                                                  1920,
                                                  960,
                                                  0,
                                                  0,
                                                  90,
                                                  90);
  assert(input_x == 960 && int(input_y) == 413);
  
  tie(input_x, input_y) = NormalToEquirectangular(960,
                                                  900,
                                                  1920,
                                                  1920,
                                                  1920,
                                                  960,
                                                  0,
                                                  0,
                                                  90,
                                                  90);
  assert(int(input_x) == 826 && input_y == 480);
  
  tie(input_x, input_y) = NormalToEquirectangular(960,
                                                  1020,
                                                  1920,
                                                  1920,
                                                  1920,
                                                  960,
                                                  0,
                                                  0,
                                                  90,
                                                  90);
  assert(int(input_x) == 1093 && input_y == 480);
}

int main(int argc, char** argv) {
  CommonInit();
  test_NormalToEquirectangular();
  test_NormalToEquirectangularSymmetry();
  test_NormalToEquirectangularRectangularInput();
  return (EXIT_SUCCESS);
}

