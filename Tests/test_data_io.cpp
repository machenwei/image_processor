/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   test_data_io.cpp
 * Author: machenwei
 *
 * Created on February 28, 2018, 8:14 PM
 */
#define IS_UNIT_TEST

#include <cassert>
#include <stdlib.h>
#include <iostream>

#include "Common.h"
#include "Debug.h"

#include "DataInput.h"
#include "DataOutput.h"
#include "Processor.h"
#include "ProcessorExecutor.h"
#include "ProcessorExecutorThread.h"
/*
 * Simple C++ Test Suite
 */

class IntProcessor : public Processor {
 public:
  IntProcessor() {}
  virtual ~IntProcessor() {};
  bool ready_to_run() override {
    ready_to_run_called_ = true;
    return true;
  };
  void process() override {};
  void initialize() override {}; 
  
  bool ready_to_run_called_ = false;
};

void test_Data_Input_Output_Integration() {
  DataOutput<int> int_output(nullptr);
  DataInput<int> int_input;

  int_input.subscribe(int_output);
  auto l = int_output.get_lock();
  int_output.get_mutable() = 4;
  int_output.provide();
  assert(int_input.has_pending_updates());
  assert(int_input.get() == 4);
}

void test_DataIO_Callbacks() {
  DataOutput<int> int_output(nullptr);
  DataInput<int> int_input;

  bool provider_consumption_callback_called = false;
  bool provider_update_callback_called = false;
  bool consumer_subscribe_callback_called = false;
  bool consumer_data_income_callback_called = false;
  
  int_output.register_consumption_callback([&](){
    provider_consumption_callback_called = true;});
  int_output.register_update_callback(nullptr, [&](){
    provider_update_callback_called = true;});

  int_input.register_subscribe_notification_callback([&](){
    consumer_subscribe_callback_called = true;});
  int_input.register_data_income_callback([&](){
    consumer_data_income_callback_called = true;});

  int_input.subscribe(int_output);
  assert(consumer_subscribe_callback_called);
  {
    auto l = int_output.get_lock();
    int_output.get_mutable() = 4;
    int_output.provide();
  }
  assert(provider_update_callback_called);
  assert(consumer_data_income_callback_called);
  
  assert(int_input.has_pending_updates());
  {
    auto l = int_input.get_lock();
    assert(int_input.get() == 4);
    assert(provider_consumption_callback_called);
  }
}

void test_Processor() {
  IntProcessor p;
  p.set_name("haha");
  assert(p.name() == "haha");
  
  p.datainput_reporting_being_subscriber();
  assert(p.has_subscribing_input());
  
  p.datainput_reporting_update();
  
  p.dataoutput_reporting_being_consumed();
}

int main(int argc, char** argv) {
  CommonInit();
  test_Data_Input_Output_Integration();
  test_DataIO_Callbacks();
  test_Processor();
  return (EXIT_SUCCESS);
}

