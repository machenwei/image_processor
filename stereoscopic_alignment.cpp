/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <execinfo.h>
#include <iostream>
#include <memory>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <vector>

#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/video/background_segm.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"

#include "Debug.h"
#include "ProcessorExecutor.h"
#include "ProcessorExecutorThread.h"
#include "ProcessorExecutorThreadPool.h"

#include "Common.h"
#include "CameraSource2.h"
#include "CustomTransform.h"
#include "DisplayWindow.h"
#include "FileVideoSource.h"
#include "FlowBlur.h"
#include "FlowSynchronizer.h"
#include "ForegroundExtractor.h"
#include "FrameMotionAnalyzer.h"
#include "Gear360FrameSplit.h"
#include "ImageTransformMathFunctions.h"
#include "ResizeAbsolute.h"
#include "StereoscopicMerge.h"
#include "VideoFileSink.h"
#include "ThreadPoolInterface.h"

using namespace std;
using namespace cv;

/* Some how the fgbg module keeps running without anything upsteam module
 * invoking it with new data. A cin is put in the filesource module to step\
 * through the frames. A lot of debug prints are put in place.
 * Next Step: 
 * 1. Maybe instrument the modules so that the debug message can be
 * more informative.
 * 2. Figure out WTF is going on with this fgbg module. Why does it fucking get
 * in the run queue all the times.
 * 
 */

static void help() {
  DCOUT("\nDo background segmentation, especially demonstrating the use of cvUpdateBGStatModel().\n"
    "Learns the background at the start and then segments.\n"
    "Learning is togged by the space key. Will read from file or camera\n"
    "Usage: \n"
    "			./bgfg_segm [--camera]=<use camera, if this key is present>, [--file_name]=<path to movie file> \n\n");
}

// For smb mounts, /run/user/1000/gvfs/smb-share:server=utility01,share=mp
const char* keys = {
  "{l  left             | /home/machenwei/development/recorocer/image_processor/TestVideo/HomeTestLeft.MP4 | Left eye Gear 360 file}"
  "{r  right            | /home/machenwei/development/recorocer/image_processor/TestVideo/HomeTestRight.MP4 | Right eye Gear 360 file) }"
  "{o  output           | output123.avi | output file path }"
  "{s  use_front_side   | | Use front side }"
  "{c  cmd_mode         | | Disable preview windows }"
};

int main(int argc, const char** argv) {
  DCOUT(cv::getBuildInformation());

  CommonInit();
  help();

  const int flow_size = 200;
  
  CommandLineParser parser(argc, argv, keys);
  bool use_front_side = parser.has("use_front_side");
  bool cmd_mode = parser.has("cmd_mode");
  const string left_video_file_path = parser.get<string>("left");
  const string right_video_file_path = parser.get<string>("right");
  const string output_video_file_path = parser.get<string>("output");

  parser.printMessage();

  // Create run environment. This is the root of all thread pool, threads,
  // executors and processors.
  auto run_environment = make_unique<RunEnv>();
  
  // Create the executor.
  // Executor holds processors.
  // Each executor must specify a executor thread pool. For now it is one thread
  // per executor(type ProcessorExecutorThread). In the future there might be a
  // pool of of threads for an executor.
  ProcessorExecutor* executor_file = 
    run_environment->create_executor<ProcessorExecutorThread>("executor_file");
  ProcessorExecutor* executor_left = 
    run_environment->create_executor<ProcessorExecutorThread>("executor_left");
  ProcessorExecutor* executor_right = 
    run_environment->create_executor<ProcessorExecutorThread>("executor_right");
  ProcessorExecutor* executor_left1 = 
    run_environment->create_executor<ProcessorExecutorThread>("executor_left");
  ProcessorExecutor* executor_right1 = 
    run_environment->create_executor<ProcessorExecutorThread>("executor_right");
  ProcessorExecutor* executor_left2 = 
    run_environment->create_executor<ProcessorExecutorThread>("executor_left");
  ProcessorExecutor* executor_right2 = 
    run_environment->create_executor<ProcessorExecutorThread>("executor_right");
  
  
  ProcessorExecutor* executor_final = 
    run_environment->create_executor<ProcessorExecutorThread>("executor_final");
  ProcessorExecutor* gui_executor = 
    run_environment->create_gui_executor("gui_executor");

  // First section setup the processors and their dependencies.
  auto* file_source_left =
    executor_file->create_eager_processor<FileVideoSource>(
      "L1-file_source_left",
      left_video_file_path);
  auto* file_source_right =
    executor_file->create_eager_processor<FileVideoSource>(
      "R1-file_source_right",
      right_video_file_path);
  
  auto split_left =
    executor_left->create_lazy_processor<Gear360FrameSplit>("L2-split_left");
  split_left->raw_image_input().subscribe(file_source_left->image_output());

  auto split_right =
    executor_right->create_lazy_processor<Gear360FrameSplit>("R2-split_right");
  split_right->raw_image_input().subscribe(file_source_right->image_output());

  auto left_small = executor_left->create_lazy_processor<ResizeAbsolute>(
    "L3-resize_left",
    flow_size, flow_size);
  left_small->image_input().subscribe(split_left->front_image_output());
  
  auto right_small = executor_right->create_lazy_processor<ResizeAbsolute>(
    "R3-resize_right",
    flow_size, flow_size);
  right_small->image_input().subscribe(split_right->front_image_output());

  auto custom_transform_left =
    executor_left->create_lazy_processor<CustomTransform>(
      "3-custom_transform_left",
      [](int x, int y, int d_w, int d_h, int s_w, int s_h) -> pair<int, int> {
        return FishEyeToEquirectangular(x, y, d_w, d_h, s_w, s_h,
          /*h_fov_degree=*/190,
          /*v_fov_degree=*/190);
    });
  custom_transform_left->image_input().subscribe(
    left_small->image_output());

  auto custom_transform_right = 
    executor_right->create_lazy_processor<CustomTransform>(
      "3-custom_transform_right",
      [](int x, int y, int d_w, int d_h, int s_w, int s_h) {
        return FishEyeToEquirectangular(x, y, d_w, d_h, s_w, s_h,
          /*h_fov_degree=*/190,
          /*v_fov_degree=*/190);
    });
  custom_transform_right->image_input().subscribe(
    right_small->image_output());
  
  auto motion_left =
    executor_left1->create_lazy_processor<FrameMotionAnalyzer>("L4-motion_left");
  motion_left->image_input().subscribe(left_small->image_output());
  auto motion_right =
    executor_right1->create_lazy_processor<FrameMotionAnalyzer>("R4-motion_right");
  motion_right->image_input().subscribe(right_small->image_output());
  
  auto blured_motion_left = 
    executor_left2->create_lazy_processor<FlowBlur>("L5-flow-blur-left");
  blured_motion_left->flow_input().subscribe(motion_left->flow_output());
  blured_motion_left->set_blur_window_widht_height(0.1, 0.02);
  
  auto blured_motion_right = 
    executor_right2->create_lazy_processor<FlowBlur>("R5-flow-blur-right");
  blured_motion_right->flow_input().subscribe(motion_right->flow_output());
  blured_motion_right->set_blur_window_widht_height(0.1, 0.02);
  
  auto flow_syncer =
    executor_final->create_lazy_processor<FlowSynchronizer>("Z6-flow_syncer");
  flow_syncer->flow_input_left().subscribe(
    blured_motion_left->flow_output());
  flow_syncer->flow_input_right().subscribe(
    blured_motion_right->flow_output());
  
  auto stereo_merger = 
    executor_final->create_lazy_processor<StereoscopicMerge>("Z7-stereo_merger");
  stereo_merger->left_image_input().subscribe(
    motion_left->image_output());
  stereo_merger->right_image_input().subscribe(
    motion_right->image_output());

  auto display_window = gui_executor->create_lazy_processor<DisplayWindow>(
    "Z8-display_window", // internal name
    "Image");  // title 
  display_window->image_input().subscribe(
    stereo_merger->image_output());

  run_environment->run();
  run_environment.reset();
  return 0;
}