# python generic_auto_build.py /run/user/1000/gvfs/smb-share:server=utility01,share=mp/HomeIphoneTest/ /run/user/1000/gvfs/smb-share:server=utility01,share=mp/tmp/

import os
import sys
import time
import subprocess
import signal

FOV_MAP = {
"iphone8p":"35x62.6",
"iphone6s":"32x52",
"GoPro6Wide4:3": "127x95",
"GoPro6Wide16:9:": "122x75",
}


class Alarm(Exception):
    pass

def alarm_handler(signum, frame):
    raise Alarm

signal.signal(signal.SIGALRM, alarm_handler)

def alarm_handler(signum, frame):
    raise Alarm

def sort_key(file):
  metadata = get_metadata(file)
  if 'creation_epoch' in metadata:
    return metadata['creation_epoch']
  else:
    return ''

def find_files(root):
  media_files = []
  media_file_pairs = []
  for subdir, dirs, files in os.walk(root):
    for file in files:
      if file.endswith(".MOV") or file.endswith(".MP4"):
        media_files.append(os.path.join(subdir, file))
        print os.path.join(subdir, file), sort_key(os.path.join(subdir, file))
  sorted_media_files = sorted(media_files, key=sort_key)
  
  last_size = 1
  last_file = ""
  last_side = ""
  for file in sorted_media_files:
    metadata = get_metadata(file)
    if 'creation_epoch' not in metadata:
      continue
    size = metadata['creation_epoch']
    side = ""
    if "left" in str.lower(file):
      side = "l"
    elif "right" in str.lower(file):
      side = "r"
    else:
      continue
    if size - last_size < 60:
       media_file_pairs.append({last_side:last_file, side:file})
    last_size = size
    last_file = file
    last_side = side
  #print media_file_pairs
  #print "Done w/ media file pairs"
  return media_file_pairs

def combine_video(left_file, right_file, output_file, offset = 0):
  l_metadata = get_metadata(left_file)
  r_metadata = get_metadata(right_file)
  
  cmd = "./GenericStereo180Builder -l=%s -r=%s -d=%s -o=%s --lfov=%s --rfov=%s" % (left_file, right_file, offset, output_file, l_metadata['FOV'], r_metadata['FOV'])
  print cmd
  os.system(cmd)
  
def find_offset(left_file, right_file):
  l_metadata = get_metadata(left_file)
  r_metadata = get_metadata(right_file)
  
  cmd = "./GenericStereoAlignment -l=%s -r=%s --lfov=%s --rfov=%s" % (left_file, right_file, l_metadata['FOV'], r_metadata['FOV'])

  result = None
  for i in range(0,2):
    try:
      signal.alarm(5*60)
      result = subprocess.check_output(cmd, shell=True)
      signal.alarm(0)
    except Alarm:
      print "Timeout, killed stuck process"
      pass
  if result is None:
    return 0  

  offset = None;
  for line in result.split('\n'):
    if line.startswith('Offset='):
      print "offset is", line[7:]
      offset = line[7:]
  return offset

def run_cmd_with_timeout(cmd, timeout, attempts):
  result = None
  for i in range(0,attempts):
    try:
      signal.alarm(timeout)
      result = subprocess.check_output(cmd, shell=True)
      signal.alarm(0)
    except Alarm:
      print "Timeout, killed stuck process"
      pass
  if result is None:
    return 0  

  return result

def get_metadata(file):
  cmd = "ffmpeg -i %s -f ffmetadata /dev/null -y 2>&1" % file
  metadata = {}
  print cmd
  try:
    result = run_cmd_with_timeout(cmd, 3, 1)
    for line in result.split('\n'):
      if 'creation_time' in line:
        creation_time = ":".join(line.split(':')[1:])
        metadata['creation_time'] = creation_time
        metadata['creation_epoch'] = int(time.mktime(time.strptime(creation_time, ' %Y-%m-%d %H:%M:%S')))

  except:
    print "Metadata cmd failed: ", cmd
    pass

  for key in FOV_MAP:
    if key in file:
      metadata['FOV'] = FOV_MAP[key]

  return metadata


def add_audio(video, audio, output):
  cmd = "ffmpeg -i %s -i %s -map 0:v -map 1:a -c copy -shortest %s" % (video, audio, output)
  os.system(cmd)

def strip_audio(video, output):
  cmd = "ffmpeg -i %s -c copy -an %s" % (video, output)
  os.system(cmd)

if __name__ == "__main__":
  input_file_folder = sys.argv[1]
  output_file_folder = sys.argv[2]
  files = find_files(input_file_folder)
  for input_pair in files:
    time.sleep(0.2)
    if 'l' not in input_pair or 'r' not in input_pair:
      print "Unpaired file", input_pair
      continue 
    left = input_pair['l']
    right = input_pair['r']
    no_audio_left = input_pair['l'] + 'no_audio.MP4'
    no_audio_right = input_pair['r'] + 'no_audio.MP4'
    output = "output_" + left.split('/')[-1].split('.')[0] + '-' + right.split('/')[-1].split('.')[0]
    print output

    output_avi = os.path.join(output_file_folder, output + '.avi')
    output_mp4 = os.path.join(output_file_folder, output + '.MP4')

    print "------------------------"
    print "left: " + left
    print "right: " + right

    if os.path.isfile(output_mp4):
      print "output file exist, skipped..."
      continue

    # strip audio before processing. OpenCV has trouble dealing with GoPro audio
    # and would stop reading after 50 or so frames.
    strip_audio(left, no_audio_left)
    strip_audio(right, no_audio_right)

    offset = find_offset(no_audio_left, no_audio_right)
    if offset is None:
      continue
    
    offset = int(offset)

    combine_video(no_audio_left, no_audio_right, output_avi, offset)
    if offset > 0:    
      add_audio(output_avi, right, output_mp4)
    else:
      add_audio(output_avi, left, output_mp4)
    if os.path.isfile(output_avi):
      os.remove(output_avi)
    if os.path.isfile(no_audio_left):
      os.remove(no_audio_left)
    if os.path.isfile(no_audio_right):
      os.remove(no_audio_right)

    print "output file: " + output_mp4


