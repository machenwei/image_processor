import os
import sys
import time
import subprocess
import signal

class Alarm(Exception):
    pass

def alarm_handler(signum, frame):
    raise Alarm

signal.signal(signal.SIGALRM, alarm_handler)

def alarm_handler(signum, frame):
    raise Alarm

def sort_key(file):
  key = "/".join(file.split('/')[:-2]) + str(os.path.getsize(file))
  print key
  return key

def find_files(root):
  media_files = []
  media_file_pairs = []
  for subdir, dirs, files in os.walk(root):
    for file in files:
      if file.endswith(".MP4"):
        media_files.append(os.path.join(subdir, file))
        #print os.path.join(subdir, file), os.path.getsize(os.path.join(subdir, file))
  sorted_media_files = sorted(media_files, key=sort_key)
  
  last_size = 1
  last_file = ""
  last_side = ""
  for file in sorted_media_files:
    size = os.path.getsize(file)
    side = ""
    if "left" in str.lower(file):
      side = "l"
    elif "right" in str.lower(file):
      side = "r"
    else:
      continue
    if 1.01 > 1.0*size/last_size > 0.99:
       media_file_pairs.append({last_side:last_file, side:file})
    last_size = size
    last_file = file
    last_side = side
  return media_file_pairs

def combine_video(left_file, right_file, output_file, offset = 0):
  cmd = "./Stereo180Builder -l=%s -r=%s -d=%s -o=%s" % (left_file, right_file, offset, output_file)
  print cmd
  os.system(cmd)
  
def find_offset(left_file, right_file):
  cmd = "./StereoAlignment -l=%s -r=%s" % (left_file, right_file)

  result = None
  for i in range(0,2):
    try:
      signal.alarm(5*60)
      result = subprocess.check_output(cmd, shell=True)
      signal.alarm(0)
    except Alarm:
      print "Timeout, killed stuck process"
      pass
  if result is None:
    return 0  

  offset = None;
  for line in result.split('\n'):
    if line.startswith('Offset='):
      print "offset is", line[7:]
      offset = line[7:]
  return offset

def add_audio(video, audio, output):
  cmd = "ffmpeg -i %s -i %s -map 0:v -map 1:a -c copy -shortest %s" % (video, audio, output)
  print cmd
  os.system(cmd)

if __name__ == "__main__":
  input_file_folder = sys.argv[1]
  output_file_folder = sys.argv[2]
  files = find_files(input_file_folder)
  for input_pair in files:
    time.sleep(0.2)
    if 'l' not in input_pair or 'r' not in input_pair:
      continue 
    left = input_pair['l']
    right = input_pair['r']
    output = "output_" + left.split('/')[-1].split('.')[0] + '-' + right.split('/')[-1].split('.')[0]
    print output

    output_avi = os.path.join(output_file_folder, output + '.avi')
    output_mp4 = os.path.join(output_file_folder, output + '.MP4')

    print "------------------------"
    print "left: " + left
    print "right: " + right

    if os.path.isfile(output_mp4):
      print "output file exist, skipped..."
      continue

    offset = find_offset(left, right)
    if offset is None:
      continue
    
    offset = int(offset)

    combine_video(left, right, output_avi, offset)
    if offset > 0:    
      add_audio(output_avi, right, output_mp4)
    else:
      add_audio(output_avi, left, output_mp4)
    if os.path.isfile(output_avi):
      os.remove(output_avi)

    print "output file: " + output_mp4


