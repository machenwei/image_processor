/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <execinfo.h>
#include <iostream>
#include <memory>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <vector>

#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/video/background_segm.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"

#include "Debug.h"
#include "ProcessorExecutor.h"
#include "ProcessorExecutorThread.h"
#include "ProcessorExecutorThreadPool.h"

#include "Common.h"
#include "CameraSource2.h"
#include "CustomTransform.h"
#include "DisplayWindow.h"
#include "FileVideoSource.h"
#include "ForegroundExtractor.h"
#include "Gear360FrameSplit.h"
#include "ResizeAbsolute.h"
#include "StereoscopicMerge.h"
#include "VideoFileSink.h"
#include "ThreadPoolInterface.h"

#include "ImageTransformMathFunctions.h"

using namespace std;
using namespace cv;

/* Some how the fgbg module keeps running without anything upsteam module
 * invoking it with new data. A cin is put in the filesource module to step\
 * through the frames. A lot of debug prints are put in place.
 * Next Step: 
 * 1. Maybe instrument the modules so that the debug message can be
 * more informative.
 * 2. Figure out WTF is going on with this fgbg module. Why does it fucking get
 * in the run queue all the times.
 * 
 */

static void help() {
  DCOUT("\nDo background segmentation, especially demonstrating the use of cvUpdateBGStatModel().\n"
    "Learns the background at the start and then segments.\n"
    "Learning is togged by the space key. Will read from file or camera\n"
    "Usage: \n"
    "			./bgfg_segm [--camera]=<use camera, if this key is present>, [--file_name]=<path to movie file> \n\n");
}

// For smb mounts, /run/user/1000/gvfs/smb-share:server=utility01,share=mp
const char* keys = {
  "{l  left             | /home/machenwei/development/recorocer/image_processor/TestVideo/HomeTestLeft.MP4 | Left eye Gear 360 file}"
  "{r  right            | /home/machenwei/development/recorocer/image_processor/TestVideo/HomeTestRight.MP4 | Right eye Gear 360 file) }"
  "{o  output           | output123.avi | output file path }"
  "{s  use_front_side   | | Use front side }"
  "{c  cmd_mode         | | Disable preview windows }"
  "{d  delay            | 0 | Delay left video by n frames. Negative value delays right video }"
};

int main(int argc, const char** argv) {
  DCOUT(cv::getBuildInformation());

  CommonInit();
  help();

  CommandLineParser parser(argc, argv, keys);
  bool use_front_side = parser.has("use_front_side");
  bool cmd_mode = parser.has("cmd_mode");
  int delay = parser.get<int>("delay");
  const string left_video_file_path = parser.get<string>("left");
  const string right_video_file_path = parser.get<string>("right");
  const string output_video_file_path = parser.get<string>("output");
  
  parser.printMessage();

  int delay_left = 0;
  int delay_right = 0;
  if (delay > 0) {
    delay_left = delay;
  } else if (delay < 0) {
    delay_right = -delay;
  }
  
  // Create run environment. This is the root of all thread pool, threads,
  // executors and processors.
  RunEnv run_environment;
  
  // Create the executor.
  // Executor holds processors.
  // Each executor must specify a executor thread pool. For now it is one thread
  // per executor(type ProcessorExecutorThread). In the future there might be a
  // pool of of threads for an executor.
  ProcessorExecutor* executor_file = 
    run_environment.create_executor<ProcessorExecutorThread>("executor_file");
  ProcessorExecutor* executor_left = 
    run_environment.create_executor<ProcessorExecutorThread>("executor_left");
  ProcessorExecutor* executor_right = 
    run_environment.create_executor<ProcessorExecutorThread>("executor_right");
  ProcessorExecutor* executor_final = 
    run_environment.create_executor<ProcessorExecutorThread>("executor_final");
  ProcessorExecutor* gui_executor = 
    run_environment.create_gui_executor("gui_executor");

  // First section setup the processors and their dependencies.
  auto* file_source_left =
    executor_file->create_eager_processor<FileVideoSource>(
      "1-file_source_left",
      left_video_file_path);
  file_source_left->skip_frames(delay_left);
  auto* file_source_right =
    executor_file->create_eager_processor<FileVideoSource>(
      "1-file_source_right",
      right_video_file_path);
  file_source_right->skip_frames(delay_right);
  
  auto split_left =
    executor_left->create_lazy_processor<Gear360FrameSplit>("2-split_left");
  split_left->raw_image_input().subscribe(file_source_left->image_output());

  auto split_right =
    executor_right->create_lazy_processor<Gear360FrameSplit>("2-split_right");
  split_right->raw_image_input().subscribe(file_source_right->image_output());

  auto custom_transform_left =
    executor_left->create_lazy_processor<CustomTransform>(
      "3-custom_transform_left",
      [](int x, int y, int d_w, int d_h, int s_w, int s_h) -> pair<int, int> {
        return FishEyeToEquirectangular(x, y, d_w, d_h, s_w, s_h,
          /*h_fov_degree=*/190,
          /*v_fov_degree=*/190);
    });
  custom_transform_left->image_input().subscribe(
    split_left->front_image_output());

  
  auto custom_transform_right = 
    executor_right->create_lazy_processor<CustomTransform>(
      "3-custom_transform_right",
      [](int x, int y, int d_w, int d_h, int s_w, int s_h) {
        return FishEyeToEquirectangular(x, y, d_w, d_h, s_w, s_h,
          /*h_fov_degree=*/190,
          /*v_fov_degree=*/190);
    });
  custom_transform_right->image_input().subscribe(
    split_right->front_image_output());

  auto stereo_merger = 
    executor_final->create_lazy_processor<StereoscopicMerge>("4-stereo_merger");
  stereo_merger->left_image_input().subscribe(
    custom_transform_left->image_output());
  stereo_merger->right_image_input().subscribe(
    custom_transform_right->image_output());

  auto output_resize = executor_final->create_lazy_processor<ResizeAbsolute>(
    "5-output_resize",
    1000, 500);
  output_resize->image_input().subscribe(stereo_merger->image_output());

  auto file_sink = executor_final->create_lazy_processor<VideoFileSink>(
    "6-file sink",
    output_video_file_path);
  file_sink->image_input().subscribe(stereo_merger->image_output());

  auto display_window = gui_executor->create_lazy_processor<DisplayWindow>(
    "6-display_window", // internal name
    "Image");  // title 

  display_window->image_input().subscribe(
    output_resize->image_output());


  run_environment.run();
  
  run_environment.print_executor_state();
  
  return 0;
}
