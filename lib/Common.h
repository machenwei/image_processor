/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   common.h
 * Author: machenwei
 *
 * Created on February 21, 2018, 9:11 PM
 */

#ifndef COMMON_H
#define COMMON_H

// Disable to turn off GUI
#define ENABLE_GUI

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <execinfo.h>
#include <cxxabi.h>

extern bool global_sig_done;

void print_stacktrace(FILE *out, unsigned int max_frames);

void print_stacktrace();

void segfault_handler(int sig);

void CommonInit();

#endif /* COMMON_H */

