/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ProcessorExecutor.h
 * Author: machenwei
 *
 * Created on January 14, 2018, 2:16 PM
 */

#ifndef PROCESSOREXECUTOR_H
#define PROCESSOREXECUTOR_H

#include <thread>
#include <chrono>
#include <memory>
#include <mutex>
#include <vector>
#include <queue>
#include <functional>

#include "Processor.h"

class ProcessorExecutor {
 public:
  ProcessorExecutor();
  ProcessorExecutor(const ProcessorExecutor& orig);
  virtual ~ProcessorExecutor();
  
  // Use this for processors the data generation of which is driven by external
  // resources, eg a camera.
  void add_eager_processor(std::unique_ptr<Processor> processor);
  // Use this for processors driven by data input.
  void add_lazy_processor(std::unique_ptr<Processor> processor);
  
  template <typename P, typename ...Params>
  P* create_eager_processor(const string& name, Params&&... params) {
    P* return_value;
    auto processor = make_unique<P>(forward<Params>(params)...);
    processor->set_name(name);
    return_value = processor.get();
    add_eager_processor(move(processor));
    return return_value;
  };
  
  template <typename P, typename ...Params>
  P* create_lazy_processor(const string& name, Params&&... params) {
    P* return_value;
    auto processor = make_unique<P>(forward<Params>(params)...);
    processor->set_name(name);
    return_value = processor.get();
    add_lazy_processor(move(processor));
    return return_value;
  };
 
  bool queue_is_empty();
  
  void initialize();
  
  void execute();
  
  void print_executor_state();
  
  void set_queue_nolonger_empty_callback(std::function<void (void)> callback) {
    queue_nolonger_empty_ = callback;
  }
  
  void set_name(const std::string& name) {
    name_ = name;
  }

  void processor_is_ready(Processor* processor_ptr);

 private:
 
  // Processors should be called when its inputs are updated.
  // Processors decide if it has met the critierion to run.
  // If so, Processor call executor to schedule itself.
  
  std::vector<std::unique_ptr<Processor>> eager_processors_;
  std::vector<std::unique_ptr<Processor>> lazy_processors_;
  std::queue<Processor*> run_queue_;
  std::function<void (void)> queue_nolonger_empty_;
  
  std::mutex queue_mutext_;
  
  std::string name_;
};

#endif /* PROCESSOREXECUTOR_H */

