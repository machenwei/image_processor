/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   datainput_test.cpp
 * Author: machenwei
 *
 * Created on January 9, 2018, 9:51 PM
 */

#include <stdlib.h>
#include <iostream>

/*
 * Simple C++ Test Suite
 */

void test1() {
    std::cout << "datainput_test test 1" << std::endl;
}

void test2() {
    std::cout << "datainput_test test 2" << std::endl;
    std::cout << "%TEST_FAILED% time=0 testname=test2 (datainput_test) message=error message sample" << std::endl;
}

int main(int argc, char** argv) {
    std::cout << "%SUITE_STARTING% datainput_test" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    std::cout << "%TEST_STARTED% test1 (datainput_test)" << std::endl;
    test1();
    std::cout << "%TEST_FINISHED% time=0 test1 (datainput_test)" << std::endl;

    std::cout << "%TEST_STARTED% test2 (datainput_test)\n" << std::endl;
    test2();
    std::cout << "%TEST_FINISHED% time=0 test2 (datainput_test)" << std::endl;

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

