/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Processor.h
 * Author: machenwei
 *
 * Created on January 9, 2018, 10:24 PM
 */

#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <string>
#include <vector>

#include "Debug.h"
#include "Common.h"

using namespace std;

class ProcessorExecutor;

class Processor {
 public:
  Processor() {} ;
  Processor(const Processor& orig) {};
  virtual ~Processor() {
    DCOUT(name() << " destruct, run count " << run_count_);
  };

  // Initialize the resources in the final running context. Note this could
  // be on a different thread.
  virtual void initialize() = 0;
  
  void set_name(const std::string& name);
  
  const string& name();
  
  bool has_subscribing_input() {
    return has_subscribing_inputs_;
  }
  
  // Called to notify executor to run this processor.
  void datainput_reporting_update();
  void datainput_reporting_being_subscriber();
  void dataoutput_reporting_being_consumed();
  
  void register_processor_ready_callback(function<void(void)> callback);
 
 protected:
  void global_stop() {
    global_sig_done = true;
  }
  
 private:
  // Override this to define when the process is ready to run, typically by
  // examining inputs and outputs.
  // It is very important that some criterion is set. If this function returns
  // true when no valid update is present, the processor will run very
  // aggressively and consume all available system resource.
  virtual bool ready_to_run() = 0;

  // Assume calling Process will retrieve inputs and 
  virtual void process() = 0;
  
  vector<function<void(void)>> processor_ready_callbacks_;
  string name_ = "unnamed";
  bool has_subscribing_inputs_ = false;
  
  // Functions below are only supposed to be called from ProcessorExecutor.
  // TODO(chenwei): figure out how to restrict that without having to include
  // the class.
  friend class ProcessorExecutor;
  void process_internal();
  bool ready_to_run_internal();
  void register_executor(ProcessorExecutor* executor);
  
  int run_count_ = 0;
};

#endif /* PROCESSOR_H */

