/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ProcessorExecutorThreadPool.cpp
 * Author: machenwei
 * 
 * Created on February 13, 2018, 11:46 PM
 */

#include "ProcessorExecutorThreadPool.h"

#include "Common.h"
#include "ProcessorExecutor.h"
#include "Processor.h"

#include <stdio.h>
#include <iostream>
#include <queue>

using namespace std;

ProcessorExecutorThreadPool::ProcessorExecutorThreadPool(
    std::unique_ptr<ProcessorExecutor> executor, int pool_size) :
        executor_(std::move(executor)),
        pool_size_(pool_size){
  executor_->set_queue_nolonger_empty_callback(
    [this]{
      queue_condition_variable_.notify_all();
    });
}

ProcessorExecutorThreadPool::~ProcessorExecutorThreadPool() {
}

void ProcessorExecutorThreadPool::start() {
  cout << "Thread Pool Initializing..." << endl;
  executor_->initialize();
  cout << "Thread Pool Initialized." << endl;
  
  for (int i; i < pool_size_; i++) {
    cout << "Thread " << i << " created" << endl;
    runners_.push_back(std::make_unique<std::thread>(
      &ProcessorExecutorThreadPool::run, this));
  }
}

void ProcessorExecutorThreadPool::stop() {
  stop_sig_received_ = true;
  queue_condition_variable_.notify_all();
}

void ProcessorExecutorThreadPool::join() {
  for (auto& runner : runners_) {
    runner->join();
  }
}

void ProcessorExecutorThreadPool::run() {
  try {
    while (!stop_sig_received_) {
      executor_->execute();
      if (executor_->queue_is_empty()) {
        std::unique_lock<std::mutex> lk(queue_lock_);
        queue_condition_variable_.wait(
          lk, 
          [this]{return !executor_->queue_is_empty() || stop_sig_received_;});
      }
    }
  } catch (const std::exception& e) {
    std::cout << "Exception: " << e.what() << '\n';
    print_stacktrace();
    exit(1);
  }
}

