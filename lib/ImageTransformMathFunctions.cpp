/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "ImageTransformMathFunctions.h"

#include "opencv2/core.hpp"

using namespace std;
using namespace cv;

#define PI 3.1415926535897932384626433
pair<float, float> FishEyeToEquirectangular(int dest_x,
                                            int dest_y,
                                            int dest_width,
                                            int dest_height,
                                            int source_width,
                                            int source_height,
                                            int h_fov_degree,
                                            int v_fov_degree) {
  //Point2f pfish;
  float theta, phi, r;
  Point3f psph;
  //float FOV = PI;
  float FOV_H = (float) PI / 180 * h_fov_degree;
  float FOV_V = (float) PI / 180 * v_fov_degree;
  float width = source_width;
  float height = source_height;
  theta = PI * (dest_x / width - 0.5); // -pi to pi
  phi = PI * (dest_y / height - 0.5); // -pi/2 to pi/2
  psph.x = cos(phi) * sin(theta);
  psph.y = cos(phi) * cos(theta);
  psph.z = sin(phi);
  theta = atan2(psph.z, psph.x);
  phi = atan2(sqrt(psph.x * psph.x + psph.z * psph.z), psph.y);
  r = width * phi / FOV_H;
  float r2 = height * phi / FOV_V;
  float source_x = 0.5 * width + r * cos(theta);
  float source_y = 0.5 * height + r2 * sin(theta);
  return pair<float, float>(source_x, source_y);
}

pair<float, float> NormalToEquirectangular(int dest_x,
                                           int dest_y,
                                           int dest_width,
                                           int dest_height,
                                           int source_width,
                                           int source_height,
                                           float source_theta,
                                           float source_phi,
                                           float source_h_fov_degree,
                                           float source_v_fov_degree) {
  const float rotation = -0.5;
  float theta, phi, r;
  float FOV_H = (float) PI / 180 * source_h_fov_degree;
  float FOV_V = (float) PI / 180 * source_v_fov_degree;
  float s_width = source_width;
  float s_height = source_height;
  float d_width = dest_width;
  float d_height = dest_height;
  
  // pos of destination pixel in question
  // horizontal off center angle, possitive on right side
  theta = PI * (float(dest_x) / d_width -0.5); // -pi/2 to pi/2
  // vertical off center angle, positive at bottom
  phi = PI * (float(dest_y) / d_height -0.5); // -pi/2 to pi/2
  
  float source_x;
  float source_y;  
  
  // rotate 90 degrees for iphone
  source_x = s_width * (0.5 + 0.5 / sin(FOV_V/2) * sin(phi));
  source_y = s_height * (0.5 - 0.5 / sin(FOV_H/2) * sin(theta));

  // Normal  
  source_x = s_width * (0.5 + 0.5 / sin(FOV_H/2) * sin(theta));
  source_y = s_height * (0.5 + 0.5 / sin(FOV_V/2) * sin(phi));
  
  return pair<float, float>(source_x, source_y);
}

pair<float, float> ReSize(int dest_x,
                          int dest_y,
                          int dest_width,
                          int dest_height,
                          int source_width,
                          int source_height) {
  
  float source_x = float(dest_x) / float(dest_width) * float(source_width);
  float source_y = float(dest_y) / float(dest_height) * float(source_height);
  return pair<float, float>(source_x, source_y);
}