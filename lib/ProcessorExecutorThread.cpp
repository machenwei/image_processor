/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ProcessorExecutorThread.cpp
 * Author: machenwei
 * 
 * Created on January 10, 2018, 11:31 PM
 */

#include "ProcessorExecutorThread.h"

#include <stdio.h>
#include <iostream>
#include <queue>

#include "ProcessorExecutor.h"
#include "Processor.h"

using namespace std;

ProcessorExecutorThread::ProcessorExecutorThread() {
  
}

ProcessorExecutorThread::ProcessorExecutorThread(
    std::unique_ptr<ProcessorExecutor> executor) :
        executor_(std::move(executor)) {
  executor_->set_queue_nolonger_empty_callback(
    [this]{
      queue_condition_variable_.notify_all();
    });
}

ProcessorExecutorThread::ProcessorExecutorThread(
    const ProcessorExecutorThread& orig) {
}

ProcessorExecutorThread::~ProcessorExecutorThread() {
}

void ProcessorExecutorThread::start() {
  executor_->initialize();
  runner_ = std::make_unique<std::thread>(&ProcessorExecutorThread::run, this);
}

void ProcessorExecutorThread::stop() {
  stop_sig_received_ = true;
  queue_condition_variable_.notify_all();
}

void ProcessorExecutorThread::join() {
  runner_->join();
}

void ProcessorExecutorThread::print_processor_info(){
  executor_->print_executor_state();
}

void ProcessorExecutorThread::run() {
  while (!stop_sig_received_) {
    executor_->execute();
    if (executor_->queue_is_empty()) {
      std::unique_lock<std::mutex> lk(queue_lock_);
      queue_condition_variable_.wait(
        lk, 
        [this]{return !executor_->queue_is_empty() || stop_sig_received_;});
    }
  }
}

