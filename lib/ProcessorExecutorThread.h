/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ProcessorExecutorThread.h
 * Author: machenwei
 *
 * Created on January 10, 2018, 11:31 PM
 */

#ifndef PROCESSOREXECUTORTHREAD_H
#define PROCESSOREXECUTORTHREAD_H

#include <thread>
#include <chrono>
#include <memory>
#include <vector>
#include <queue>
#include <mutex>
#include <condition_variable>

#include "Processor.h"

class ProcessorExecutor;

class ProcessorExecutorThread {
 public:
  ProcessorExecutorThread();
  ProcessorExecutorThread(std::unique_ptr<ProcessorExecutor> executor_);
  ProcessorExecutorThread(const ProcessorExecutorThread& orig);
  virtual ~ProcessorExecutorThread();
  
  void start();
  void stop();
  void join();
  
  void print_processor_info();
  
 private:

  void run();
  bool stop_sig_received_ = false;
  std::unique_ptr<std::thread> runner_;
  
  std::queue<Processor*> run_queue_;
  
  std::unique_ptr<ProcessorExecutor> executor_;
  
  std::mutex queue_lock_;
  std::condition_variable queue_condition_variable_;
};

#endif /* PROCESSOREXECUTORTHREAD_H */

