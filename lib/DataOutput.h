/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DataOutput.h
 * Author: machenwei
 *
 * Created on January 9, 2018, 9:25 PM
 */

#ifndef DATAOUTPUT_H
#define DATAOUTPUT_H

#include <iostream>
#include <memory>
#include <mutex>
#include <vector>
#include <unordered_set>
#include <cassert>

#include "Debug.h"
#include "Processor.h"

using namespace std;

class DataOutputInterface {
  virtual bool has_pending_subscriber() = 0;
};

template<typename T>
class DataOutput {
 public:
  DataOutput(Processor* processor) : data_(make_unique<T>()),
    parent_name_(processor->name()){
    if (processor != nullptr) {
      register_consumption_callback([processor](){
        processor->dataoutput_reporting_being_consumed();});
    }
  };

  virtual ~DataOutput() {
    DCOUT(parent_name_ << " " << typeid(this).name()
         << " destruct, output consumed: "
         << is_ready_for_new_value());
  };

  // DataInput at subscriber use this
  const T& get(void* subscriber_ptr) {
    DASSERTE(pending_subscriber_count_ > 0,
             "get from subscriber should not access input before value is available");
    DASSERTE(data_mutext_.try_lock() == false,
             "get() must be called while holding lock.");
    // Only subscriber are allowed to access value by get(void*). Watcher should
    // user get();
    DASSERTE(subscriber_list_.find(subscriber_ptr)!= subscriber_list_.end(),
             "Only subscriber are allowed to access value by get(void*)");
    DASSERTE(subscriber_list_.find(subscriber_ptr)!= pending_subscriber_list_.end(),
             "Caller of get not a pending subscriber");
    // should not add here. Only subscriber should count. Processors who receive
    // no notification 
    pending_subscriber_count_--;
    pending_subscriber_list_.erase(subscriber_ptr);
    for (auto consumption_callback : consumption_callbacks_) {
      consumption_callback();
    }
    
    return *data_;
  };

// DataInput as watcher use this
  const T& get() const {
    // caller of get() must aquire lock before doing so.
    //DASSERTE(data_mutext_.try_lock() == false,
    //         "Only subscriber call get_lock() before calling get()");
    // should not add here. Only subscriber should count. Processors who receive
    // no notification 
    return *data_;
  };  
  
  T& get_mutable() {
    // caller of get_mutable() must aquire lock before doing so.
    assert(data_mutext_.try_lock() == false);
    return *data_;
  };
  
  // Register a callback to get a notification that a new output is available.
  void register_update_callback(void* subscriber, 
                                function<void(void)> callback) {
    update_callbacks_.push_back(callback);
    subscriber_list_.insert(subscriber);
  }
  
  // Register a callback to get a notification that a new output is available.
  void register_consumption_callback(function<void(void)> callback) {
    consumption_callbacks_.push_back(callback);
  }
  
  void provide() {
    //DASSERTE(data_mutext_.try_lock() == false,
    //         "provide() must be called while holding lock");
    // arbitrary limit on pending subscriber count. Prevent indefinit wait. 
    DASSERTE(pending_subscriber_count_ == 0,
             "should not provide value before last is consumed");
    pending_subscriber_count_ += subscriber_list_.size();
    pending_subscriber_list_ = subscriber_list_;
    for (auto update_callback : update_callbacks_) {
      update_callback();
    }
  }
  
  unique_ptr<lock_guard<mutex>> get_lock() {
    return make_unique<lock_guard<mutex>>(data_mutext_);
  }
  
  bool is_ready_for_new_value() const {
    DASSERTE(pending_subscriber_count_ >= 0, "pending subscriber can't be <0");
    return pending_subscriber_count_ == 0;
  }
  
 private:
  unique_ptr<T> data_;
  vector<function<void(void)>> update_callbacks_;
  vector<function<void(void)>> consumption_callbacks_;
  
  unordered_set<void*> subscriber_list_;
  unordered_set<void*> pending_subscriber_list_;
  mutex data_mutext_;
  
  int pending_subscriber_count_ = 0;
  const string& parent_name_;
};
#endif /* DATAOUTPUT_H */

