/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Processor.cpp
 * Author: machenwei
 * 
 * Created on January 9, 2018, 10:24 PM
 */

#include "Debug.h"
#include "Processor.h"
#include "ProcessorExecutor.h"

void Processor::register_executor(ProcessorExecutor* executor) {
  register_processor_ready_callback([executor, this](){
    executor->processor_is_ready(this);});
}

void Processor::datainput_reporting_update() {
  DCOUT(name_ << " input updated");
  // only report ready to run if
  // 1. All inputs are ready.
  // 2. Previous outputs are all consumed. 
  if (ready_to_run()) {
    for (auto& processor_ready_callback : processor_ready_callbacks_) {
      processor_ready_callback();
    }
  }
}

void Processor::dataoutput_reporting_being_consumed() {
  // only report ready to run if
  // 1. All inputs are ready.
  // 2. Previous outputs are all consumed. 
  DCOUT(name_ << " dataoutput_reporting_being_consumed() called");
  if (ready_to_run()) {
    DCOUT(name_ << " dataoutput_reporting_being_consumed() reporting processor ready");
    for (auto& processor_ready_callback : processor_ready_callbacks_) {
      processor_ready_callback();
    }
  }
}

void Processor::datainput_reporting_being_subscriber() {
  has_subscribing_inputs_ = true;
}

void Processor::set_name(const std::string& name) {
  name_ = name;
}

const std::string& Processor::name() {
  return name_;
}

bool Processor::ready_to_run_internal() {
  return ready_to_run();
}

void Processor::process_internal() {
  // Very important, do not allow running processor without it being ready.
  // inside of process assumes things are ready. We could mess up a lot of stuff
  // on the subscribing part if we force process.
  if (!ready_to_run()) return;
  TIMER(name_);
  if (has_subscribing_inputs_) {
    DCOUT(name_ << " process() running");
  }
  process();
  if (has_subscribing_inputs_) {
    DCOUT(name_ << " process() done");
  }
  run_count_ ++;
}

void Processor::register_processor_ready_callback(
    function<void(void)> callback) {
  processor_ready_callbacks_.push_back(callback);
}