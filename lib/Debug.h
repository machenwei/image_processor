/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Debug.h
 * Author: machenwei
 *
 * Created on February 2, 2018, 5:19 PM
 */

#ifndef DEBUG_H
#define DEBUG_H

//Overall Debug Flag
//#define DEBUG
#define PROFILE
#define DEBUG_ASSERT

// Simple cout for debug
#ifdef DEBUG
#define DCOUT( x ) std::cout << x << std::endl
#else
#define DCOUT( x ) 
#endif // DEBUG

#ifdef DEBUG_ASSERT
#include <cassert>
#include "Common.h"
#define DASSERT( x ) if(!(x)) print_stacktrace(); assert(x)
#define DASSERTE( x, y ) if(!(x)) print_stacktrace(); assert(x && y)
#else
#define DASSERT( x )
#endif

// Timer
#ifdef PROFILE
#include <iostream>
#include <unordered_map>
#include <map>
#include <chrono>
#include <string>
#include <iomanip>
#include <mutex>

class TimeKeeper {
 public:
  TimeKeeper() {};
  ~TimeKeeper() {
    std::unique_lock<std::mutex> lk(time_keeper_lock_);
    for (auto const& x : total_time_) {
      std::cout << std::left << std::setw(45) << x.first 
                << " cnt:" << std::setw(5) <<  sample_count_[x.first] 
                << " total:" << std::setw(15) << x.second.count() << "s" 
                << " avg:" << std::setw(10) << x.second.count()/sample_count_[x.first]*1000 << "ms" 
                
                << std::endl;
    }
  };

 private:
  std::map<std::string, std::chrono::duration<double>> total_time_;
  std::unordered_map<std::string, long> sample_count_;
  std::mutex time_keeper_lock_;
  friend class Timer;
};

static TimeKeeper _time_keeper;

class Timer {
 public:
  Timer(const std::string& name) :
      name_(name),
      start_(std::chrono::high_resolution_clock::now()) {
  };
  ~Timer() {
    using dura = std::chrono::duration<double>;
    auto d = std::chrono::high_resolution_clock::now() - start_;
    std::lock_guard<std::mutex> lk(_time_keeper.time_keeper_lock_);
    _time_keeper.sample_count_[name_] ++;
    _time_keeper.total_time_[name_] += std::chrono::duration_cast<dura>(d);
  };
 private:
  const std::string name_;
  const std::chrono::high_resolution_clock::time_point start_;

};
// A timer is not thread safe. Only use a timer to measure the execution of a
// scoped block of code 
#define TIMER(name) Timer _timer_instance(name)

#else
#define TIMER(name)
#endif /* PROFILE */

#endif /* DEBUG_H */

