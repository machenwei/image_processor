/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DataInput.h
 * Author: machenwei
 *
 * Created on January 9, 2018, 9:24 PM
 */

#ifndef DATAINPUT_H
#define DATAINPUT_H

#include <memory>
#include <mutex>
#include <vector>

#include "DataOutput.h"

#include "Debug.h"

using namespace std;

class Processor;

template<typename T>
class DataInput {
 public:
  DataInput() : parent_name_("") {};
  DataInput(Processor* processor) : parent_name_(processor->name()) {
    register_subscribe_notification_callback([processor](){
      processor->datainput_reporting_being_subscriber();});
    register_data_income_callback([processor](){
      processor->datainput_reporting_update();});
  };

  DataInput(const DataInput& orig) {
  };

  virtual ~DataInput() {
    DCOUT(parent_name_ << " " << typeid(this).name() << " destruction, " 
         << "input available: " << has_pending_updates());
  };

  // Connect input to a source as subscriber. Input gets "source_updated()" ping
  // when source updates. And source would hold processing before the data is
  // consumed.
  void subscribe(DataOutput<T>& source) {
    data_source_ = &source;
    is_subscriber_ = true;
    for (auto subscribe_notification_callback : subscribe_notification_callbacks_) {
      subscribe_notification_callback();
    }
    source.register_update_callback(
      this, [this]{input_source_report_value_updated();});
  }
  
  // Connect input to a source as a reference. Input added as reference does not
  // get source update pings. And the source does not hold processing for
  // consumer
  void refer_to(DataOutput<T>& source) {
    data_source_ = &source;
    is_subscriber_ = false;
  }
  
  // Input source calls this to notify consumer that the data is updated.
  void input_source_report_value_updated() {
    pending_updates_ ++;
    for (auto data_income_callback : data_income_callbacks_) {
      data_income_callback();
    }
  }

  unique_ptr<lock_guard<mutex>> get_lock() {
    return move(data_source_->get_lock());
  }
  
  // Check if update is available. Mostly from ready_to_run() method of a process
  // This can be triggered by data source sending notifications, or by consumer
  // calling the get(this) on DataOutput.
  bool has_pending_updates() {
    return pending_updates_ > 0 || !is_subscriber_;
  }
  
  // Subscriber must call DataOutput::get(DataInput*). Otherwise the output
  // would consider subscriber as still pending and may refuse to process new
  // data.
  // IMPORTANT: Only call it once! Calling multiple times will reduce the frame
  // rate of the subsequent processors.
  const T& get() {
    if (is_subscriber_) {
      // The original thought was that subscriber should not call
      // get when there is no update. The reservation was that a
      // processor could try to subscribe on 2 inputs. While the
      // "and" or "or" logic can be easily defined in the is_ready
      // method, a "or" would trigger a subscribed input being read
      // before there is an update.
      // So on a second through, why would I care if a get is called
      // more than once? So hell with it I am lifting the assertion.

      // Weird changing this to = 0 causes the pipe to get stuck...
      // Important not allowing this to go blow 0. If a processor
      // reads the input more than once and this goes below 0, subsequent
      // updates may not trigger processing and frame rates will drop
      // from where that happened.
      DASSERTE(has_pending_updates(), "No update available");
      if (pending_updates_ > 0) {
        pending_updates_ --;
      }
      return data_source_->get(this);
    } else {
      return data_source_->get();
    }
  }
  
  void register_subscribe_notification_callback(function<void(void)> callback) {
    subscribe_notification_callbacks_.push_back(callback);
  }
  
  void register_data_income_callback(function<void(void)> callback) {
    data_income_callbacks_.push_back(callback);
  }
  
 private:
  DataOutput<T>* data_source_;
  
  vector<function<void(void)>> subscribe_notification_callbacks_;
  vector<function<void(void)>> data_income_callbacks_;
  
  bool is_subscriber_ = false;
  int pending_updates_ = 0;
  
  const string& parent_name_;
};

#endif /* DATAINPUT_H */

