/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ProcessorExecutor.cpp
 * Author: machenwei
 * 
 * Created on January 14, 2018, 2:16 PM
 */

#include "ProcessorExecutor.h"

#include <iostream>
#include <queue>
#include <stdio.h>

#include "Debug.h"
#include "Processor.h"

using namespace std;

ProcessorExecutor::ProcessorExecutor() {
}

ProcessorExecutor::ProcessorExecutor(
    const ProcessorExecutor& orig) {
}

ProcessorExecutor::~ProcessorExecutor() {
  DCOUT("processor executor destruct " << name_);
}

void ProcessorExecutor::print_executor_state() {
  for (const auto& processor : eager_processors_) {
    cout << processor->name() << " ready status: "
         << processor->ready_to_run_internal() << endl;
  }
  for (const auto& processor : lazy_processors_) {
    cout << processor->name() << " ready status: "
         << processor->ready_to_run_internal() << endl;
  }
}

// Eager processor cannot have any subscribed inputs.
void ProcessorExecutor::add_eager_processor(
    unique_ptr<Processor> processor) {
  // Eager processor shouldn't have any subscribing input.
  // It just doesn't make sense.
  DASSERTE(!processor->has_subscribing_input(), 
           "Eager processor shouldn't have any subscribing input.");
  processor->register_executor(this);
  eager_processors_.push_back(std::move(processor));
}

void ProcessorExecutor::add_lazy_processor(
    unique_ptr<Processor> processor) {
  processor->register_executor(this);
  lazy_processors_.push_back(std::move(processor));
}

void ProcessorExecutor::initialize() {
  for (auto& processor_ptr : eager_processors_) {
    processor_ptr->initialize();
  }
  for (auto& processor_ptr : lazy_processors_) {
    processor_ptr->initialize();
  }
}

// This method need to be thread safe.
void ProcessorExecutor::execute() {
  Processor* p = nullptr;
  {
    std::lock_guard<std::mutex> l(queue_mutext_);
    if (run_queue_.size() > 0) {
      p = run_queue_.front();
      run_queue_.pop();
    }
  }
  if (p != nullptr) {
    p->process_internal();
  }
  
  std::lock_guard<std::mutex> l(queue_mutext_);
  if (run_queue_.size() == 0) {
    for (auto& processor_ptr : eager_processors_) {
      if (processor_ptr->ready_to_run_internal()) {
        run_queue_.push(processor_ptr.get());
      }
    }
  }
}

void ProcessorExecutor::processor_is_ready(Processor* processor_ptr) {
  {
    std::lock_guard<std::mutex> l(queue_mutext_);
    run_queue_.push(processor_ptr);
  }
  if (queue_nolonger_empty_!= nullptr) {
    queue_nolonger_empty_();
  }
}

bool ProcessorExecutor::queue_is_empty() {
  std::lock_guard<std::mutex> l(queue_mutext_);
  return run_queue_.size() == 0;
}