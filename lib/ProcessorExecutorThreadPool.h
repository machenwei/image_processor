/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ProcessorExecutorThreadPool.h
 * Author: machenwei
 *
 * Created on February 13, 2018, 11:46 PM
 */

#ifndef PROCESSOREXECUTORTHREADPOOL_H
#define PROCESSOREXECUTORTHREADPOOL_H

#include <thread>
#include <chrono>
#include <memory>
#include <vector>
#include <queue>
#include <mutex>
#include <condition_variable>

#include "Processor.h"

class ProcessorExecutor;

class ProcessorExecutorThreadPool {
 public:
  ProcessorExecutorThreadPool() = delete;
  ProcessorExecutorThreadPool(const ProcessorExecutorThreadPool& orig) = delete;
  
  ProcessorExecutorThreadPool(std::unique_ptr<ProcessorExecutor> executor_, 
                              int pool_size);
  
  virtual ~ProcessorExecutorThreadPool();

  void start();
  void stop();
  void join();
  
  void add_eager_processor(std::unique_ptr<Processor> processor);
  void add_lazy_processor(std::unique_ptr<Processor> processor);
  
 private:
  void initialize();
  void run();
  bool stop_sig_received_ = false;
  std::vector<std::unique_ptr<std::thread>> runners_;
  
  std::vector<std::unique_ptr<Processor>> eager_processors_;
  std::vector<std::unique_ptr<Processor>> lazy_processors_;
  std::queue<Processor*> run_queue_;
  
  std::unique_ptr<ProcessorExecutor> executor_;
  
  std::mutex queue_lock_;
  std::condition_variable queue_condition_variable_;

  // Currently this only works if the size is 1.Debug is messing this up.
  // a race condition is probably messing up the alloc size. I got bad_alloc
  // Sometimes DataInput assertion 'pending_updates_ > 0' also fails.
  // Had a couple of good runs with pool size = 2. Not sure if ProcessorExecutor.cpp:59 
  // Processor* p initialize to nullptr fixed to problem. Need to verify with more runs.
  const int pool_size_;
};

#endif /* PROCESSOREXECUTORTHREADPOOL_H */

