/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ThreadPoolInterface.h
 * Author: machenwei
 *
 * Created on April 9, 2018, 7:16 PM
 */

#ifndef THREADPOOLINTERFACE_H
#define THREADPOOLINTERFACE_H

#include <memory>
#include <stdio.h>
#include <vector>

using namespace std;

class ThreadPoolInterface {
 public:
  virtual ~ThreadPoolInterface() {};
  virtual void start() = 0;
  virtual void stop() = 0;
  virtual void join() = 0;
  virtual void print_executor_state() = 0;
};

template<typename THREAD_POOL_TYPE>
class ThreadPoolImpl : public ThreadPoolInterface {
 public:
  ThreadPoolImpl(unique_ptr<THREAD_POOL_TYPE> thread_pool) :
  thread_pool_(move(thread_pool)) {
  };

  // Alternatively create the thread pool impl directly from executor

  template<typename EXECUTOR_TYPE, typename ...Args>
  ThreadPoolImpl(EXECUTOR_TYPE executor, Args... args) :
  thread_pool_(make_unique<THREAD_POOL_TYPE>(move(executor), args...)) {
  };
  
  ~ThreadPoolImpl() override {
  };

  void start() override {
    thread_pool_->start();
  };

  void stop() override {
    thread_pool_->stop();
  };

  void join() override {
    thread_pool_->join();
  };

  void print_executor_state() override {
    thread_pool_->print_processor_info();
  };
  
 private:
  unique_ptr<THREAD_POOL_TYPE> thread_pool_;
};

class RunEnv {
 public:

  RunEnv() : gui_executor_(std::make_unique<ProcessorExecutor>()) {
  };

  ~RunEnv() {
  };

  template <typename THREAD_POOL_TYPE>
  ProcessorExecutor* create_executor(const string& name) {
    ProcessorExecutor* return_value;

    unique_ptr<ProcessorExecutor> executor =
      std::make_unique<ProcessorExecutor>();
    executor->set_name(name);

    return_value = executor.get();

    executor_thread_pools_.push_back(
      make_unique<ThreadPoolImpl < THREAD_POOL_TYPE >> (move(executor)));
    return return_value;
  }

  ProcessorExecutor* create_gui_executor(const string& name) {
    gui_executor_->set_name(name);
    return gui_executor_.get();
  }

  void run() {
    gui_executor_->initialize();
    for (auto& thread_pool : executor_thread_pools_) {
      thread_pool->start();
    }
    for (;;) {
      gui_executor_->execute();
      char k = (char) waitKey(20);
      if (k == 27 || global_sig_done) {
        break;
      }
      if (k == ' ') {
      }
    }
    for (auto& thread_pool : executor_thread_pools_) {
      thread_pool->stop();
    }
    for (auto& thread_pool : executor_thread_pools_) {
      thread_pool->join();
    }
  }
  
  void print_executor_state() {
    for (const auto& thread_pool : executor_thread_pools_) {
      thread_pool->print_executor_state();
    }
    gui_executor_->print_executor_state();
  }

 private:
  vector<unique_ptr<ThreadPoolInterface>> executor_thread_pools_;

  std::unique_ptr<ProcessorExecutor> gui_executor_;
};

#endif /* THREADPOOLINTERFACE_H */

