/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ImageTransformMathFunctions.h
 * Author: machenwei
 *
 * Created on April 9, 2018, 7:24 PM
 */

#ifndef IMAGETRANSFORMMATHFUNCTIONS_H
#define IMAGETRANSFORMMATHFUNCTIONS_H

#include <utility>

#define PI 3.1415926535897932384626433
std::pair<float, float> FishEyeToEquirectangular(int dest_x,
                                                 int dest_y,
                                                 int dest_width,
                                                 int dest_height,
                                                 int source_width,
                                                 int source_height,
                                                 int h_fov_degree,
                                                 int v_fov_degree);

std::pair<float, float> NormalToEquirectangular(int dest_x,
                                                int dest_y,
                                                int dest_width,
                                                int dest_height,
                                                int source_width,
                                                int source_height,
                                                float source_theta,
                                                float source_phi,
                                                float source_h_fov_degree,
                                                float source_v_fov_degree);

std::pair<float, float> ReSize(int dest_x,
                               int dest_y,
                               int dest_width,
                               int dest_height,
                               int source_width,
                               int source_height);
#endif /* IMAGETRANSFORMMATHFUNCTIONS_H */

