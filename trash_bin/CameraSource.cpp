/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CameraSource.cpp
 * Author: machenwei
 * 
 * Created on January 7, 2018, 2:55 PM
 */

#include "CameraSource.h"

#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/video/background_segm.hpp"
#include "opencv2/videoio.hpp"

#include <stdio.h>
#include <iostream>


using namespace std;
using namespace cv;

CameraSource::CameraSource() {
}

CameraSource::CameraSource(const CameraSource& orig) {
}

CameraSource::~CameraSource() {
}

void CameraSource::start() {
    runner = std::make_unique<std::thread>(&CameraSource::run, this);
}

void CameraSource::stop() {
    stop_sig_received = true; 
}

void CameraSource::join(int deadline_ms) {
    runner->join();
}

bool CameraSource::is_ready(bool blocking, int deadline_ms) {
    if( !cap.isOpened() ) {
        return false;
    } else {
        return true;
    }
}

void CameraSource::run() {
    cap.open(0);
    if( !is_ready(false, 0) ) {
        printf("can not open camera or video file\n");
        return;
    } else {
        cout << "Video " <<
                ": width=" << cap.get(CAP_PROP_FRAME_WIDTH) <<
                ", height=" << cap.get(CAP_PROP_FRAME_HEIGHT) <<
                ", nframes=" << cap.get(CAP_PROP_FRAME_COUNT) << std::endl;
    }
    Mat tmp_img;
    while (!stop_sig_received) {
        cap >> tmp_img;
        {
          std::lock_guard<std::mutex> guard(img_mutex);
          tmp_img.copyTo(img);
        }
    }
}

const Mat& CameraSource::get_img() {
    std::lock_guard<std::mutex> guard(img_mutex);
    return img;
}