/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ProcessorInterface.h
 * Author: machenwei
 *
 * Created on January 7, 2018, 3:36 PM
 */

#ifndef PROCESSORINTERFACE_H
#define PROCESSORINTERFACE_H

class ProcessorInterface {
public:
    virtual void start() = 0;
    virtual void stop() = 0;
    virtual void join(int deadline_sec) = 0;
    virtual bool is_ready(bool blocking, int deadline_ms) = 0;
};

#endif /* PROCESSORINTERFACE_H */

