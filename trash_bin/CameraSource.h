/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CameraSource.h
 * Author: machenwei
 *
 * Created on January 7, 2018, 2:55 PM
 */

#ifndef CAMERASOURCE_H
#define CAMERASOURCE_H

#include <thread>
#include <chrono>
#include <memory>
#include <mutex>

#include "opencv2/videoio.hpp"
#include "ProcessorInterface.h"

using namespace cv;
using namespace std;

class CameraSource : public ProcessorInterface {
 public:
  CameraSource();
  CameraSource(const CameraSource& orig);
  virtual ~CameraSource();

  void start() override;
  void stop() override;
  void join(int deadline_sec) override;
  bool is_ready(bool blocking, int deadline_ms) override;

  const Mat& get_img();

 private:
  // Main loop to be forked in the dedicated thread.
  void run();

  VideoCapture cap;
  Mat img;

  bool stop_sig_received = false;

  std::unique_ptr<std::thread> runner;
  mutex img_mutex;
};

#endif /* CAMERASOURCE_H */

