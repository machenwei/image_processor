/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdio.h>
#include <iostream>
#include <memory>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/video/background_segm.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"

#include "Common.h"
#include "Debug.h"
#include "ProcessorExecutor.h"
#include "ProcessorExecutorThread.h"

#include "DisplayWindow.h"
#include "CameraSource2.h"
#include "FileVideoSource.h"
#include "ForegroundExtractor.h"
#include "Gear360FrameSplit.h"
#include "StereoscopicMerge.h"
#include "CustomTransform.h"
#include "ResizeAbsolute.h"
#include "ObjectDetector.h"
#include "FrameMotionAnalyzer.h"
#include "FlowSynchronizer.h"
#include "FlowBlur.h"

using namespace std;
using namespace cv;

/* Somehow image frame rate got messed up if both object detector and fgbg are
 * running. Remove fgbg and it becomes fine.
 * And cpu is not fully utilized. Try with more thread.
 */

static void help() {
  DCOUT("\nDo background segmentation, especially demonstrating the use of cvUpdateBGStatModel().\n"
    "Learns the background at the start and then segments.\n"
    "Learning is togged by the space key. Will read from file or camera\n"
    "Usage: \n"
    "			./bgfg_segm [--camera]=<use camera, if this key is present>, [--file_name]=<path to movie file> \n\n");
}

const char* keys = {
  "{c  camera   |         | use camera or not}"
  "{m  method   |mog2     | method (knn or mog2) }"
  "{s  smooth   |         | smooth the mask }"
  "{fn file_name|../data/tree.avi | movie file        }"
};

int main(int argc, const char** argv) {
  DCOUT(cv::getBuildInformation());

  CommonInit();
  help();

  CommandLineParser parser(argc, argv, keys);
  bool useCamera = parser.has("camera");
  bool smoothMask = parser.has("smooth");
  string file = parser.get<string>("file_name");
  string method = parser.get<string>("method");

  parser.printMessage();
  
  auto camera_source = make_unique<CameraSource2>(0, /*device id*/ 
                                                  300, /*width*/
                                                  200 /*height*/);
  camera_source->set_name("1 camera_source");
  
  auto motion_analyzer = make_unique<FrameMotionAnalyzer>();
  motion_analyzer->image_input().subscribe(
    camera_source->image_output());
  motion_analyzer->set_name("2 frame_motion_analyzer");
  
 
  auto flow_blur_left = make_unique<FlowBlur>();
  flow_blur_left->flow_input().subscribe(
    motion_analyzer->flow_output());
  flow_blur_left->set_name("3.1 flow blur left");
  
  auto flow_blur_right = make_unique<FlowBlur>();
  flow_blur_right->flow_input().subscribe(
    motion_analyzer->flow_output());
  flow_blur_right->set_name("3.2 flow blur right");
  
  
  auto flow_syncer = make_unique<FlowSynchronizer>();
  flow_syncer->flow_input_left().subscribe(
    flow_blur_left->flow_output());
  flow_syncer->flow_input_right().subscribe(
    flow_blur_right->flow_output());
  flow_syncer->set_name("4.1 flow_syncer");
  
  auto display_cam_raw = make_unique<DisplayWindow>("display_cam_raw");
  display_cam_raw->image_input().refer_to(
    motion_analyzer->image_output());
  display_cam_raw->set_name("4 display_cam_raw");
  
  // Now setup the executors. This in principle should be only about graph
  // execution performance. Eg we should have about as many threads as there
  // are CPU cores. 
  std::unique_ptr<ProcessorExecutor> executor_cam =
    std::make_unique<ProcessorExecutor>();
  executor_cam->add_eager_processor(move(camera_source));
  executor_cam->add_lazy_processor(move(motion_analyzer));

  executor_cam->add_lazy_processor(move(flow_syncer));
  executor_cam->set_name("executor_cam");
  
  std::unique_ptr<ProcessorExecutor> executor_left =
    std::make_unique<ProcessorExecutor>();
  executor_left->add_lazy_processor(move(flow_blur_left));
  executor_left->set_name("exe_left");
  
  std::unique_ptr<ProcessorExecutor> executor_right =
    std::make_unique<ProcessorExecutor>();
  executor_right->add_lazy_processor(move(flow_blur_right));
  executor_right->set_name("exe_right");
  
  // executor2 is for UI thus it remains in the main thread.
  std::unique_ptr<ProcessorExecutor> gui_executor =
    std::make_unique<ProcessorExecutor>();
  gui_executor->add_eager_processor(move(display_cam_raw));

  // executor1 is pushed to a separate thread.
  ProcessorExecutorThread t1(std::move(executor_cam));
  t1.start();
  ProcessorExecutorThread t2(std::move(executor_left));
  t2.start();
  ProcessorExecutorThread t3(std::move(executor_right));
  t3.start();
  
  // There should probably be a special executor container that handles UI
  // inputs and display. For now let's just run it directly at the bottom like
  // like this.
  gui_executor->initialize();

  // Set UI interval to 50Hz
  for (;;) {
    gui_executor->execute();
    char k = (char) waitKey(20);
    if (k == 27) {
      break;
    }
    if (k == ' ') {
    }
  }

  t1.stop();
  t1.join();
  t2.stop();
  t2.join();
  t3.stop();
  t3.join();
  return 0;
}
