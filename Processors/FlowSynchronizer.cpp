/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FlowSynchronizer.
 * Author: machenwei
 * 
 * Created on April 6, 2018, 9:01 PM
 */

#include "FlowSynchronizer.h"

#include <iostream>

using namespace std;
using namespace cv;

namespace {

Mat BlurFlow(const Mat& input) {
  double min = -1, max = 1;
  minMaxLoc(input, &min, &max);
  double norm_factor = max - min;

  int sizeY = 20;
  int sizeX = 20;
  Mat output = input.clone();
  for (int y = 0; y < input.rows; y += 1) {
    for (int x = 0; x < input.cols; x += 1) {
      double x_sum = 0;
      double y_sum = 0;
      int count = 0;
      Point2f& outputatxy = output.at<Point2f>(y, x);
      for (int j = y - sizeY / 2; j < y + sizeY / 2; j += 1) {
        if (j < 0 || j >= input.rows) {
          continue;
        }
        for (int i = x - sizeX / 2; i < x + sizeX / 2; i += 1) {
          if (i < 0 || i >= input.cols) {
            continue;
          }
          const Point2f& pt = input.at<Point2f>(j, i);
          x_sum += (pt.x - min) / norm_factor * 2 - 1;
          y_sum += (pt.y - min) / norm_factor * 2 - 1;
          count += 1;
        }
      }
      //cout << x_sum / sizeY / sizeX << " " << y_sum / sizeY / sizeX << endl;
      outputatxy.x = x_sum / count;
      outputatxy.y = y_sum / count;
      if (outputatxy.x > 1 || outputatxy.y > 1) {
        cout << "value too high, max-min" << norm_factor << endl;
        cout << outputatxy.x << " " << outputatxy.y << endl;
      }
    }
  }
  return output;
}

long double similarity(const Mat& input1, const Mat& input2) {
  long double score = 0;
  long double norm_factor = input1.rows * input1.cols;
  for (int y = 0; y < input1.rows; y += 1) {
    for (int x = 0; x < input1.cols; x += 1) {
      if (input1.at<Point2f>(y, x).x > 1 || input2.at<Point2f>(y, x).x > 1 ||
        input1.at<Point2f>(y, x).y > 1 || input2.at<Point2f>(y, x).y > 1) {
        //cout << "run away value!!!x=" << x << "y=" << y << endl;
        //cout << input1.at<Point2f>(y,x).x << " "
        //     << input1.at<Point2f>(y,x).y << " "
        //     << input2.at<Point2f>(y,x).x << " "
        //     << input2.at<Point2f>(y,x).y << endl;
      }
      score += input1.at<Point2f>(y, x).x * input2.at<Point2f>(y, x).x;
      score += input1.at<Point2f>(y, x).y * input2.at<Point2f>(y, x).y;
    }
  }
  //cout << "score " << score << endl;
  //cout << "norm factor " << norm_factor << endl;
  //cout << "similarity " << score / norm_factor << endl;
  return score / norm_factor;
}

} // namespace

FlowSynchronizer::~FlowSynchronizer() {
  if (stored_flows_left_.size() > 10) {
    align(0.2, 0.5);
  }
}

void FlowSynchronizer::align(float search_range_ratio, // 0.2
                             float window_size_ratio) { // 0.5
  int sample_size = stored_flows_left_.size(); // 200
  int search_range = sample_size * search_range_ratio; // 50
  int window_size = sample_size * window_size_ratio; // 100

  long double max_score = 0;
  int best_offset = -1;
  long double confidence = 0;
  // offset here means how many frames to skip in the left video.
  for (int offset = -search_range; offset <= search_range; offset += 1) {
    long double score = 0;
    int left_window_start = sample_size / 2 - window_size / 2 + round((offset - 0.4) / 2);
    int right_window_start = sample_size / 2 - window_size / 2 - round((offset + 0.4) / 2);
    //cout << left_window_start << " " << right_window_start << endl;
    for (int cursor_pos = 0; cursor_pos < window_size; cursor_pos += 1) {
      //cout << "score of " << left_window_start + cursor_pos << " vs "
      //     << right_window_start + cursor_pos << endl;
      score += similarity(
        stored_flows_left_[left_window_start + cursor_pos],
        stored_flows_right_[right_window_start + cursor_pos]);
    }
    if (score > max_score) {
      confidence = score / max_score;
      max_score = score;
      best_offset = offset;
    }
    //cout << "offest " << offset << " " << score << endl;
  }
  if (confidence > 0 || true) {
    if (best_offset > max_offset_) max_offset_ = best_offset;
    if (best_offset < min_offset_) min_offset_ = best_offset;
    offset_sum_ += best_offset;
    offset_count_ += 1;
    //cout << "best offset " << best_offset << " confidenc " << confidence 
    //       << " score " << max_score << endl;
    cout << "Offset=" << round(offset_sum_ / offset_count_) << endl;
    if (offset_count_ > 3 && max_offset_ - min_offset_ < 2) {
      global_stop();
    }
    if (offset_count_ > 20) {
      global_stop();
    }
  }
  stored_flows_left_.erase(stored_flows_left_.begin(),
    stored_flows_left_.begin() + stored_flows_left_.size() / 2);
  stored_flows_right_.erase(stored_flows_right_.begin(),
    stored_flows_right_.begin() + stored_flows_right_.size() / 2);
}

void FlowSynchronizer::process() {
  {
    int sample_size = 200; // 200
    int search_range = 40; // 50
    int window_size = 100; // 80

    const auto& l1 = input_flow_left_.get_lock();
    const auto& l2 = input_flow_right_.get_lock();
    const auto& l3 = output_image_.get_lock();

    stored_flows_left_.push_back(input_flow_left_.get().clone());
    stored_flows_right_.push_back(input_flow_right_.get().clone());

    if (stored_flows_left_.size() > sample_size) {
      //cout << "Start shedding old record" << endl;
      stored_flows_left_.erase(stored_flows_left_.begin());
    }
    if (stored_flows_right_.size() > sample_size) {
      stored_flows_right_.erase(stored_flows_right_.begin());
    }

    if (stored_flows_left_.size() == sample_size && stored_flows_right_.size() == sample_size) {
      align(0.2, 0.5); // search range ratio, window size ratio
      /*
      long double max_score = 0;
      int best_offset = -1;
      long double confidence = 0;
      // offset here means how many frames to skip in the left video.
      for (int offset = -search_range; offset <= search_range; offset += 1) {
        long double score = 0;
        int left_window_start = sample_size / 2 - window_size / 2 + round((offset - 0.4) / 2);
        int right_window_start = sample_size / 2 - window_size / 2 - round((offset + 0.4) / 2);
        //cout << left_window_start << " " << right_window_start << endl;
        for (int cursor_pos = 0; cursor_pos < window_size; cursor_pos += 1) {
          //cout << "score of " << left_window_start + cursor_pos << " vs "
          //     << right_window_start + cursor_pos << endl;
          score += similarity(
            stored_flows_left_[left_window_start + cursor_pos],
            stored_flows_right_[right_window_start + cursor_pos]);
        }
        if (score > max_score) {
          confidence = score / max_score;
          max_score = score;
          best_offset = offset;
        }
        //cout << "offest " << offset << " " << score << endl;
      }
      if (confidence > 0 || true) {
        if (best_offset > max_offset_) max_offset_ = best_offset;
        if (best_offset < min_offset_) min_offset_ = best_offset;
        offset_sum_ += best_offset;
        offset_count_ += 1;
        //cout << "best offset " << best_offset << " confidenc " << confidence 
        //       << " score " << max_score << endl;
        cout << "Offset=" << round(offset_sum_ / offset_count_) << endl;
        if (offset_count_ > 3 && max_offset_ - min_offset_ < 2) {
          global_stop();
        }
      }
      stored_flows_left_.erase(stored_flows_left_.begin(),
        stored_flows_left_.begin() + stored_flows_left_.size() / 2);
      stored_flows_right_.erase(stored_flows_right_.begin(),
        stored_flows_right_.begin() + stored_flows_right_.size() / 2);*/
    }
    //const Mat& orignalImage = input_image_.get();
    //Mat outImage(orignalImage.rows, orignalImage.cols, CV_8UC3);
    // ... fill output
    //outImage.copyTo(output_image_.get_mutable());
  }
  //output_image_.provide();
}


