/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FileVideoSource.h
 * Author: machenwei
 *
 * Created on January 15, 2018, 10:39 PM
 */

#ifndef FILEVIDEOSOURCE_H
#define FILEVIDEOSOURCE_H

#include <string>
#include <iostream>

#include "opencv2/videoio.hpp"
#include "Processor.h"
#include "DataInput.h"
#include "DataOutput.h"

class FileVideoSource : public Processor{
 public:
  FileVideoSource(const string& file_path) : 
    file_path_(file_path), 
    image_output_(this) {};
  FileVideoSource(const FileVideoSource& orig) : 
    file_path_(orig.file_path_),
    image_output_(this) {};
  virtual ~FileVideoSource() {};
  
  void initialize() override;
  
  bool ready_to_run() override {
    return image_output_.is_ready_for_new_value();  
  };
  
  void process() override;
  
  void skip_frames(int num_to_skip);
  
  DataOutput<cv::Mat>& image_output() {
    return image_output_;
  };

 private:
  cv::VideoCapture capture_;
  cv::Mat temp_image_;
  const string file_path_;
  DataOutput<cv::Mat> image_output_;
  
  int num_frames_to_skip_ = 0;
  
  int frame_count_ = 0;
};

#endif /* FILEVIDEOSOURCE_H */

