/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StereoscopicMerge.h
 * Author: machenwei
 *
 * Created on January 25, 2018, 12:05 AM
 */

#ifndef STEREOSCOPICMERGE_H
#define STEREOSCOPICMERGE_H

#include "Processor.h"
#include "DataInput.h"
#include "DataOutput.h"

#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"

class StereoscopicMerge : public Processor {
 public:
  StereoscopicMerge() : 
    left_image_(this),
    right_image_(this),
    output_image_(this) {
  };

  StereoscopicMerge(const StereoscopicMerge& orig) : 
    left_image_(this), 
    right_image_(this),
    output_image_(this) {
  };
  
  void process() override;
  
  void initialize() override;

  bool ready_to_run() override {
    return left_image_.has_pending_updates() &&
           right_image_.has_pending_updates() &&
           output_image_.is_ready_for_new_value();  
  };
  
  DataInput<cv::Mat>& left_image_input() {
    return left_image_;
  };
  DataInput<cv::Mat>& right_image_input() {
    return right_image_;
  };
  DataOutput<cv::Mat>& image_output() {
    return output_image_;
  };  

 private:
  DataInput<cv::Mat> left_image_;
  DataInput<cv::Mat> right_image_;
  DataOutput<cv::Mat> output_image_;
};

#endif /* STEREOSCOPICMERGE_H */

