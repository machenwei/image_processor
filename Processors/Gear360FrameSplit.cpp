/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Gear360FrameSplit.cpp
 * Author: machenwei
 * 
 * Created on January 18, 2018, 11:10 PM
 */

#include "Gear360FrameSplit.h"

void Gear360FrameSplit::process() {
  {
    const auto& l1 = raw_image_.get_lock();
    const auto& l2 = front_image_.get_lock();
    const auto& l3 = back_image_.get_lock();

    cv::Mat raw_image = raw_image_.get();

    int width = raw_image.size().width / 2;
    int height = raw_image.size().height;

    cv::Rect left_box(0, 0, width, height);
    cv::Rect right_box(width, 0, width, height);
    cv::Mat right_image = raw_image(right_box);
    cv::Mat left_image = raw_image(left_box);

    left_image.copyTo(back_image_.get_mutable());
    right_image.copyTo(front_image_.get_mutable());    
  }
  front_image_.provide();
  back_image_.provide();
}

