/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FlowBlur.h
 * Author: machenwei
 * 
 * Created on April 8, 2018, 9:19 PM
 */

#ifndef FLOWBLUR_H_
#define FLOWBLUR_H_

#include <functional>
#include <iostream>

#include "Processor.h"
#include "DataInput.h"
#include "DataOutput.h"
#include "Debug.h"

#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"

class FlowBlur : public Processor {
 public:
  FlowBlur() :
  input_flow_(this),
  output_flow_(this) {
  };

  FlowBlur(const FlowBlur& orig) :
  input_flow_(this),
  output_flow_(this) {
  };

  virtual ~FlowBlur() {
  };
  
  void set_blur_window_widht_height(float width, float height) {
    blur_width_ = width;
    blur_height_ = height;
  }

  bool ready_to_run() override {
    return input_flow_.has_pending_updates() &&
           output_flow_.is_ready_for_new_value();
  };

  void process() override;

  void initialize() override {
  };

  DataInput<cv::Mat>& flow_input() {
    return input_flow_;
  };

  DataOutput<cv::Mat>& flow_output() {
    return output_flow_;
  };

 private:
  DataInput<cv::Mat> input_flow_;
  DataOutput<cv::Mat> output_flow_;
  
  float blur_width_ = 0.0;
  float blur_height_ = 0.0;
};

#endif /* FLOWBLUR_H_ */

