/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FlowBlur.cpp
 * Author: machenwei
 * 
 * Created on April 8, 2018, 9:19 PM
 */

#include "FlowBlur.h"

#include <iostream>

using namespace std;
using namespace cv;

void FlowBlur::process() {
  {
    const auto& l1 = input_flow_.get_lock();
    const auto& l2 = output_flow_.get_lock();
    
    const Mat& input = input_flow_.get();

    double min = -1, max = 1;
    //minMaxLoc(input, &min, &max);
    max = input.cols;
    min = -input.cols;
    
    double norm_factor = max - min;
    //cout << norm_factor << endl;
    //norm_factor = input.rows;
    //double norm_factor = input.cols;

    int sizeY = round(input.rows * blur_height_);
    int sizeX = round(input.cols * blur_width_);
    Mat output = input.clone();
    for (int y = 0; y < input.rows; y += 1) {
      for (int x = 0; x < input.cols; x += 1) {
        double x_sum = 0;
        double y_sum = 0;
        int count = 0;
        Point2f& outputatxy = output.at<Point2f>(y, x);
        for (int j = y - sizeY / 2; j < y + sizeY / 2; j += 1) {
          if (j < 0 || j >= input.rows) {
            continue;
          }
          for (int i = x - sizeX / 2; i < x + sizeX / 2; i += 1) {
            if (i < 0 || i >= input.cols) {
              continue;
            }
            const Point2f& pt = input.at<Point2f>(j, i);
            x_sum += (pt.x - min) / norm_factor * 2 - 1;
            y_sum += (pt.y - min) / norm_factor * 2 - 1;
            count += 1;
          }
        }
        //cout << x_sum / sizeY / sizeX << " " << y_sum / sizeY / sizeX << endl;
        outputatxy.x = x_sum / count;
        outputatxy.y = y_sum / count;
        if (outputatxy.x > 1 || outputatxy.y > 1) {
          //cout << "value too high, max-min" << norm_factor << endl;
          //cout << outputatxy.x << " " << outputatxy.y << endl;
        }
      }
    }
    // ... fill output 
    output.copyTo(output_flow_.get_mutable());
  }
  output_flow_.provide();
}


