/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FileVideoSource.cpp
 * Author: machenwei
 * 
 * Created on January 15, 2018, 10:39 PM
 */

#include "FileVideoSource.h"

#include <stdio.h>
#include <iostream>
#include <cassert>
#include <chrono>
#include <thread>

#include "opencv2/videoio.hpp"
#include "opencv2/imgproc.hpp"

#include "Debug.h"

using namespace cv;
using namespace std;

void FileVideoSource::initialize() {
  capture_.open(file_path_);
  if (!capture_.isOpened()) {
    printf("CameraSource2 cannot open camera or video file\n");
    assert(false);
  } else {
    DCOUT("FileVideoSource"
          << ": width=" << capture_.get(CAP_PROP_FRAME_WIDTH)
          << ", height=" << capture_.get(CAP_PROP_FRAME_HEIGHT));
  }
}

void FileVideoSource::process() {
  for (; num_frames_to_skip_ > 0; num_frames_to_skip_--) {
    capture_ >> temp_image_;
  }
  capture_ >> temp_image_;
  
  //int count = 0;
  //while (temp_image_.empty() && count < 10) {
  //  printf("Capture retry\n");
  //  std::this_thread::sleep_for(std::chrono::milliseconds(100));
  //  capture_ >> temp_image_;
  //  count ++;
  //}
  
  if (temp_image_.empty()) {
    printf("CameraSource2 reached end of file, terminating\n");
    global_stop();
    return;
  }
  {
    const auto& l = image_output_.get_lock();
    temp_image_.copyTo(image_output_.get_mutable());
  }
  image_output_.provide();
  frame_count_ ++;
}

void FileVideoSource::skip_frames(int num_to_skip) {
  num_frames_to_skip_ = num_to_skip;
}