/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FrameMotionAnalyzer.h
 * Author: machenwei
 *
 * Created on January 25, 2018, 8:09 PM
 */

#ifndef FRAME_MOTION_ANALYZER_H
#define FRAME_MOTION_ANALYZER_H

#include <functional>
#include <iostream>

#include "Processor.h"
#include "DataInput.h"
#include "DataOutput.h"

#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"

class FrameMotionAnalyzer : public Processor {
 public:

  FrameMotionAnalyzer() :
  input_image_(this),
  output_image_(this),
  flow_output_ (this) {
  };

  FrameMotionAnalyzer(const FrameMotionAnalyzer& orig) :
  input_image_(this),
  output_image_(this),
  flow_output_ (this) {
  };

  virtual ~FrameMotionAnalyzer() {
  };

  bool ready_to_run() override {
    return input_image_.has_pending_updates() &&
           output_image_.is_ready_for_new_value() &&
           flow_output_.is_ready_for_new_value();
  };

  void process() override;

  void initialize() override {
  };

  DataInput<cv::Mat>& image_input() {
    return input_image_;
  };

  DataOutput<cv::Mat>& image_output() {
    return output_image_;
  };
  
  DataOutput<cv::Mat>& flow_output() {
    return flow_output_;
  };

 private:
  DataInput<cv::Mat> input_image_;
  DataOutput<cv::Mat> output_image_;
  DataOutput<cv::Mat> flow_output_;
  cv::Mat prevgray_;
};

#endif /* FRAME_MOTION_ANALYZER_H */

