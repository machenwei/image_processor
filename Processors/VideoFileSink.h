/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   VideoFileSink.h
 * Author: machenwei
 *
 * Created on January 25, 2018, 8:09 PM
 */

#ifndef VIDEOFILESINK_H
#define VIDEOFILESINK_H

#include <functional>
#include <iostream>

#include "Processor.h"
#include "DataInput.h"
#include "DataOutput.h"

#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"

class VideoFileSink : public Processor {
 public:

  VideoFileSink(const string& file_name) :
    input_image_(this),
    file_name_(file_name){
  };

  VideoFileSink(const VideoFileSink& orig) :
    input_image_(this),
    file_name_(orig.file_name_){
  };

  virtual ~VideoFileSink();

  bool ready_to_run() override {
    return input_image_.has_pending_updates();
  };

  void process() override;

  void initialize() override;

  DataInput<cv::Mat>& image_input() {
    return input_image_;
  };

 private:
  DataInput<cv::Mat> input_image_;
  const string& file_name_;
  unique_ptr<cv::VideoWriter> video_sink_;

};

#endif /* VIDEOFILESINK_H */

