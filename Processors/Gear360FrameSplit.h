/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Gear360FrameSplit.h
 * Author: machenwei
 *
 * Created on January 18, 2018, 11:10 PM
 */

#ifndef GEAR360FRAMESPLIT_H
#define GEAR360FRAMESPLIT_H

#include "Processor.h"
#include "DataInput.h"
#include "DataOutput.h"

#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"

class Gear360FrameSplit : public Processor{
 public:
  Gear360FrameSplit() : 
    raw_image_(this),
    front_image_(this),
    back_image_(this) {};
  Gear360FrameSplit(const Gear360FrameSplit& orig) :
    raw_image_(this),
    front_image_(this),
    back_image_(this) {};
  virtual ~Gear360FrameSplit() {};
  
  void process() override;
  
  void initialize() override {};
  
  bool ready_to_run() override {
    return raw_image_.has_pending_updates() &&
           back_image_.is_ready_for_new_value() && 
           front_image_.is_ready_for_new_value();  
  };
  
  DataInput<cv::Mat>& raw_image_input() {
    return raw_image_;
  };
  DataOutput<cv::Mat>& front_image_output() {
    return front_image_;
  };
  DataOutput<cv::Mat>& back_image_output() {
    return back_image_;
  };
  
 private:
  DataInput<cv::Mat> raw_image_;
  DataOutput<cv::Mat> front_image_;
  DataOutput<cv::Mat> back_image_;
};

#endif /* GEAR360FRAMESPLIT_H */

