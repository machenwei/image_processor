/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CustomTransform.cpp
 * Author: machenwei
 * 
 * Created on January 25, 2018, 8:09 PM
 */

#include "CustomTransform.h"

#include <iostream>

using namespace std;
using namespace cv;

void CustomTransform::update_map(int src_width,
                                 int src_height,
                                 int dest_width,
                                 int dest_height) {
  float input_x, input_y;
  tie(input_x, input_y) = fn_(0, 10, 20, 20, 20, 20);
  
  Size dest_size(dest_width,dest_height);
  
  if (map_x_.size() == dest_size) {
    return;
  }
  map_x_.create(dest_size, CV_32FC1);
  map_y_.create(dest_size, CV_32FC1);

  for (int row = 0; row < dest_height; row++) {
    for (int col = 0; col < dest_width; col++) {
      float input_x, input_y;
      tie(input_x, input_y) = fn_(col,
        row,
        dest_width,
        dest_height,
        src_width,
        src_height);
      map_x_.at<float>(row, col) = input_x;
      map_y_.at<float>(row, col) = input_y;
    }
  }
}

void CustomTransform::process() {
  {
    const auto& l1 = input_image_.get_lock();
    const Mat& orignalImage = input_image_.get();
    update_map(orignalImage.size().width,orignalImage.size().height,1920,1920);
    Mat outImage(orignalImage.rows, orignalImage.cols, CV_8UC3);
    remap(orignalImage,
      outImage,
      map_x_,
      map_y_,
      CV_INTER_LINEAR, BORDER_CONSTANT, Scalar(0, 0, 0));
    
    const auto& l2 = output_image_.get_lock();
    outImage.copyTo(output_image_.get_mutable());
  }
  output_image_.provide();
}


