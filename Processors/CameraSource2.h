/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CameraSource2.h
 * Author: machenwei
 *
 * Created on January 10, 2018, 10:48 PM
 */

#ifndef CAMERASOURCE2_H
#define CAMERASOURCE2_H

#include "opencv2/videoio.hpp"
#include "Processor.h"
#include "DataInput.h"
#include "DataOutput.h"

class CameraSource2 : public Processor{
 public:
  CameraSource2(int device_id, 
                int frame_width, 
                int frame_height) :
    image_output_(this),
    camera_device_id_(device_id),
    camera_frame_width_(frame_width),
    camera_frame_height_(frame_height) {
  }
  CameraSource2(const CameraSource2& orig) :
    image_output_(this),
    camera_device_id_(orig.camera_device_id_),
    camera_frame_width_(orig.camera_frame_width_),
    camera_frame_height_(orig.camera_frame_height_) {
  }
  virtual ~CameraSource2() {};
  
  bool ready_to_run() override {
    return image_output_.is_ready_for_new_value();
  };

  void initialize() override;
  
  void process() override;
  
  DataOutput<cv::Mat>& image_output() {
    return image_output_;
  };
  
 private:
  cv::VideoCapture capture_;
  cv::Mat temp_image_;
  DataOutput<cv::Mat> image_output_;
  mutex image_mutext_;
  
  const int camera_device_id_;
  const int camera_frame_width_; 
  const int camera_frame_height_; 
};

#endif /* CAMERASOURCE2_H */

