/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ResizeAbsolute.cpp
 * Author: machenwei
 * 
 * Created on January 17, 2018, 8:06 PM
 */

#include "ResizeAbsolute.h"

#include <iostream>

using namespace std;

void ResizeAbsolute::initialize() {
}

void ResizeAbsolute::process() {
  {
    const auto& l1 = raw_image_.get_lock();
    const auto& l2 = resized_image_.get_lock();

    cv::Mat img;

    cv::resize(raw_image_.get(), img,
      cv::Size(width_, height_), 0, 0,
      cv::INTER_LINEAR_EXACT);

    img.copyTo(resized_image_.get_mutable());
  }
  resized_image_.provide();
}
