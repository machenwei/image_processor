/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   VideoFileSink.cpp
 * Author: machenwei
 * 
 * Created on January 25, 2018, 8:09 PM
 */

#include "VideoFileSink.h"

#include <iostream>

#include "Debug.h"

using namespace std;
using namespace cv;

VideoFileSink::~VideoFileSink() {
  video_sink_.release();
};

void VideoFileSink::initialize() {
  video_sink_ = make_unique<cv::VideoWriter> (
    file_name_,
    VideoWriter::fourcc('X','V','I','D'),
    30, 
    Size(3840,1920));
  
  if (!video_sink_->isOpened())
  {
      DCOUT("Could not open the output video for write: " << file_name_);
  }
}


void VideoFileSink::process() {  
  {
    const auto& l = input_image_.get_lock();
    const Mat input_frame = input_image_.get();
    if (input_frame.empty()) {
      return;
    }
    //cout << input_frame.size().width << endl;
    //cout << input_frame.size().height << endl;
    assert(input_frame.size().width == 3840);
    assert(input_frame.size().height == 1920);

    
    video_sink_->write(input_frame);
  }
}


