/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DisplayWindow.h
 * Author: machenwei
 *
 * Created on January 12, 2018, 10:31 PM
 */

#ifndef DISPLAYWINDOW_H
#define DISPLAYWINDOW_H

#include <string>

#include "opencv2/highgui.hpp"
#include "Processor.h"
#include "DataInput.h"
#include "DataOutput.h"

using namespace cv;
using namespace std;

class DisplayWindow : public Processor {
 public:
  DisplayWindow(const string& window_name);
  DisplayWindow(const DisplayWindow& orig);
  virtual ~DisplayWindow();
  
  void initialize() override;
  
  void process() override;
  
  bool ready_to_run() override {
    return image_input_.has_pending_updates();  
  };
  
  DataInput<Mat>& image_input() {
    return image_input_;
  }
  
 private:
  DataInput<Mat> image_input_;
  const string window_name_;
  int frame_count_ = 0;
};

#endif /* DISPLAYWINDOW_H */

