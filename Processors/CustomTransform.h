/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CustomTransform.h
 * Author: machenwei
 *
 * Created on January 25, 2018, 8:09 PM
 */

#ifndef CUSTOMTRANSFORM_H
#define CUSTOMTRANSFORM_H

#include <functional>
#include <iostream>

#include "Processor.h"
#include "DataInput.h"
#include "DataOutput.h"

#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"

class CustomTransform : public Processor {
 public:
  CustomTransform(std::function<std::pair<int,int>(int, int, int, int, int, int)> fn) : 
    input_image_(this),
    output_image_(this),
    fn_(fn){};
  CustomTransform(const CustomTransform& orig) : 
    input_image_(this), 
    output_image_(this),
    fn_(orig.fn_) {};
  virtual ~CustomTransform() {};
  
  bool ready_to_run() override {
    return input_image_.has_pending_updates() &&
           output_image_.is_ready_for_new_value();  
  };
  
  void process() override;
  
  void initialize() override {};
  
  DataInput<cv::Mat>& image_input() {
    return input_image_;
  };
  DataOutput<cv::Mat>& image_output() {
    return output_image_;
  };  

 private:
  
  void update_map(int src_width,
                  int src_height,
                  int dest_width,
                  int dest_height);
  
  DataInput<cv::Mat> input_image_;
  DataOutput<cv::Mat> output_image_;
  std::function<std::pair<int,int>(int, int, int, int, int, int)> fn_;
  
  cv::Mat map_x_, map_y_;
};

#endif /* CUSTOMTRANSFORM_H */

