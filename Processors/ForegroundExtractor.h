/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ForegroundExtractor.h
 * Author: machenwei
 *
 * Created on January 10, 2018, 9:22 PM
 */

#ifndef FOREGROUNDEXTRACTOR_H
#define FOREGROUNDEXTRACTOR_H

#include "Processor.h"
#include "DataInput.h"
#include "DataOutput.h"

#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/video/background_segm.hpp"

#include "Debug.h"

class ForegroundExtractor : public Processor {
 public:
  ForegroundExtractor();
  ForegroundExtractor(const ForegroundExtractor& orig);
  virtual ~ForegroundExtractor();

  void process() override;
  
  void initialize() override;
  
  bool ready_to_run() override {
    //DCOUT("------------------------------" << endl
    //     << "bgfg has update: " << raw_image_.update_available() << endl
    //     << "forground pending " << foreground_image_.has_pending_subscriber() << endl
    //     << "background pending " << background_image_.has_pending_subscriber() << endl
    //     << "mask pending " << foreground_mask_.has_pending_subscriber());
    return raw_image_.has_pending_updates() && 
           foreground_image_.is_ready_for_new_value() &&
           background_image_.is_ready_for_new_value() &&
           foreground_mask_.is_ready_for_new_value();  
  };
  
  DataInput<cv::Mat>& raw_image_input() {
    return raw_image_;
  };
  DataOutput<cv::Mat>& foreground_image_output() {
    return foreground_image_;
  };
  DataOutput<cv::Mat>& background_image_output() {
    return background_image_;
  };
  DataOutput<cv::Mat>& foreground_mask_output() {
    return foreground_mask_;
  };
  
 private:
  DataInput<cv::Mat> raw_image_;
  DataOutput<cv::Mat> foreground_image_;
  DataOutput<cv::Mat> background_image_;
  DataOutput<cv::Mat> foreground_mask_;
  
  cv::Ptr<cv::BackgroundSubtractor> bg_model_;
};

#endif /* FOREGROUNDEXTRACTOR_H */

