/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FlowSynchronizer.h
 * Author: machenwei
 * 
 * Created on April 6, 2018, 9:00 PM
 */

#ifndef FLOWSYNCHRONIZER_H_
#define FLOWSYNCHRONIZER_H_

#include <functional>
#include <iostream>
#include <vector>

#include "Processor.h"
#include "DataInput.h"
#include "DataOutput.h"
#include "Debug.h"

#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"

class FlowSynchronizer : public Processor {
 public:

  FlowSynchronizer() :
  input_flow_left_(this),
  input_flow_right_(this),
  output_image_(this) {
  };

  FlowSynchronizer(const FlowSynchronizer& orig) :
  input_flow_left_(this),
  input_flow_right_(this),
  output_image_(this) {
  };

  virtual ~FlowSynchronizer();

  bool ready_to_run() override {
    return input_flow_left_.has_pending_updates() &&
           input_flow_right_.has_pending_updates() &&
           output_image_.is_ready_for_new_value();
  };

  void process() override;

  void initialize() override {
  };

  DataInput<cv::Mat>& flow_input_left() {
    return input_flow_left_;
  };

  DataInput<cv::Mat>& flow_input_right() {
    return input_flow_right_;
  };

  DataOutput<cv::Mat>& image_output() {
    return output_image_;
  };

 private:
  void align(float search_range_ratio, float window_size_ratio);
  
  DataInput<cv::Mat> input_flow_left_;
  DataInput<cv::Mat> input_flow_right_;
  DataOutput<cv::Mat> output_image_;
  std::vector<cv::Mat> stored_flows_left_;
  std::vector<cv::Mat> stored_flows_right_;
  
  int max_offset_ = -100;
  int min_offset_ = 100;
  float offset_sum_ = 0;
  float offset_count_ = 0;
};

#endif /* FLOWSYNCHRONIZER_H_ */

