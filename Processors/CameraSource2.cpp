/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CameraSource2.cpp
 * Author: machenwei
 * 
 * Created on January 10, 2018, 10:48 PM
 */

#include "CameraSource2.h"

#include <stdio.h>
#include <iostream>
#include <cassert>

#include "opencv2/videoio.hpp"
#include "opencv2/imgproc.hpp"

#include "Debug.h"

using namespace cv;
using namespace std;

void CameraSource2::initialize() {
  capture_.open(0);
  if (!capture_.isOpened()) {
    printf("CameraSource2 cannot open camera or video file\n");
    assert(false);
  } else {
    capture_.set(CAP_PROP_FRAME_WIDTH, camera_frame_width_);
    capture_.set(CAP_PROP_FRAME_HEIGHT, camera_frame_height_);
    DCOUT("CameraSource2 Video "
          << ": width=" << capture_.get(CAP_PROP_FRAME_WIDTH)
          << ", height=" << capture_.get(CAP_PROP_FRAME_HEIGHT));
  }
}

void CameraSource2::process() {
  capture_ >> temp_image_;
  if (temp_image_.empty()) {
    return;
  }
  {
    const auto& l = image_output_.get_lock();
    temp_image_.copyTo(image_output_.get_mutable());
  }
  image_output_.provide();
}