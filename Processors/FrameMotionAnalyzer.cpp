/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FrameMotionAnalyzer.cpp
 * Author: machenwei
 * 
 * Created on January 25, 2018, 8:09 PM
 */

#include "FrameMotionAnalyzer.h"
#include <iostream>

#include "Debug.h"
#include "opencv2/video/tracking.hpp"

using namespace std;
using namespace cv;

string type2str(int type) {
  string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

void FrameMotionAnalyzer::process() {
  {
    const auto& l1 = input_image_.get_lock();
    const auto& l2 = output_image_.get_lock();
    const auto& l3 = flow_output_.get_lock();

    const Mat orignalImage = input_image_.get().clone();

    Mat flow, frame;
    // some faster than mat image container
    UMat flowUmat;
    Mat original;
    Mat current_gray_img;
    // save original for later
    orignalImage.copyTo(current_gray_img);
    orignalImage.copyTo(original);

    // just make current frame gray
    cvtColor(current_gray_img, current_gray_img, COLOR_BGR2GRAY);

    if (prevgray_.empty() == false) {
      // calculate optical flow 
      calcOpticalFlowFarneback(prevgray_, current_gray_img, flowUmat, 0.4, 1, 12, 2, 8, 1.2, 0);
      // copy Umat container to standard Mat
      
      flowUmat.copyTo(flow);

      // By y += 5, x += 5 you can specify the grid 
      for (int y = 0; y < original.rows; y += 5) {
        for (int x = 0; x < original.cols; x += 5) {
          // get the flow from y, x position * 10 for better visibility
          const Point2f flowatxy = flow.at<Point2f>(y, x) * 10;
          // draw line at flow direction
          line(original, Point(x, y), Point(cvRound(x + flowatxy.x), cvRound(y + flowatxy.y)), Scalar(255, 0, 0));
          // draw initial point
          circle(original, Point(x, y), 1, Scalar(0, 0, 0), -1);
        }
      }
    } 
    // fill previous image in case prevgray.empty() == true
    current_gray_img.copyTo(prevgray_);  
   
    //Mat outImage(orignalImage.rows, orignalImage.cols, CV_8UC3);
    // ... fill output
    original.copyTo(output_image_.get_mutable());
    flow.copyTo(flow_output_.get_mutable());
  }
  output_image_.provide();
  flow_output_.provide();
}

