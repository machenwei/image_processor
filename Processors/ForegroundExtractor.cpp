/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ForegroundExtractor.cpp
 * Author: machenwei
 * 
 * Created on January 10, 2018, 9:22 PM
 */

#include "ForegroundExtractor.h"

#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/video/background_segm.hpp"
#include "opencv2/opencv.hpp"

#include <stdio.h>
#include <iostream>
#include <memory>
#include <cassert>

using namespace cv;
using namespace std;

ForegroundExtractor::ForegroundExtractor() : 
  raw_image_(this), 
  foreground_image_(this),
  background_image_(this),
  foreground_mask_(this) {
}

ForegroundExtractor::ForegroundExtractor(const ForegroundExtractor& orig): 
  raw_image_(this),
  foreground_image_(this),
  background_image_(this),
  foreground_mask_(this)
   {
}

ForegroundExtractor::~ForegroundExtractor() {
}

void ForegroundExtractor::initialize() {
  bg_model_ = 
    createBackgroundSubtractorMOG2().dynamicCast<BackgroundSubtractor>();
}

void ForegroundExtractor::process() {
  {
    const auto& l1 = foreground_image_.get_lock();
    const auto& l2 = foreground_mask_.get_lock();
    const auto& l3 = background_image_.get_lock();
    const auto& l4 = raw_image_.get_lock();

    Mat img = raw_image_.get();
    //cuda::GpuMat gpu_img(img);

    if (foreground_image_.get().empty()) {
      foreground_image_.get_mutable().create(img.size(), img.type());
    }
    bg_model_->apply(img, foreground_mask_.get_mutable(), true ? -1 : 0); // update bg model

    if (true) { // smoothMask
      GaussianBlur(foreground_mask_.get_mutable(), foreground_mask_.get_mutable(),
        Size(11, 11), 3.5, 3.5);
      threshold(foreground_mask_.get_mutable(), foreground_mask_.get_mutable(), 10, 255,
        THRESH_BINARY);
    }

    foreground_image_.get_mutable() = Scalar::all(0);
    img.copyTo(foreground_image_.get_mutable(), foreground_mask_.get());
    bg_model_->getBackgroundImage(background_image_.get_mutable());
  }
  foreground_image_.provide();
  background_image_.provide();
  foreground_mask_.provide();
}

