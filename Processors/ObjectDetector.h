/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ObjectDetector.h
 * Author: machenwei
 *
 * Created on January 25, 2018, 8:09 PM
 */

#ifndef OBJECTDETECTOR_H
#define OBJECTDETECTOR_H

#include <functional>
#include <iostream>

#include "Processor.h"
#include "DataInput.h"
#include "DataOutput.h"

#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"
//#include "opencv2/cudaobjdetect.hpp"

class ObjectDetector : public Processor {
 public:
  ObjectDetector() :
  input_image_(this),
  output_image_(this) {
  };

  ObjectDetector(const ObjectDetector& orig) :
  input_image_(this),
  output_image_(this) {
  };

  virtual ~ObjectDetector() {
  };

  bool ready_to_run() override {
    return input_image_.has_pending_updates() &&
           output_image_.is_ready_for_new_value();
  };

  void process() override;

  void initialize() override {
    DCOUT("Loading Model");
    //cascade_gpu_ = cv::cuda::CascadeClassifier::create(
    //  "/home/machenwei/development/recorocer/image_processor/data/haarcascades_cuda/haarcascade_frontalface_alt_tree.xml");
    DCOUT("Model Loaded");
  };

  DataInput<cv::Mat>& image_input() {
    return input_image_;
  };

  DataOutput<cv::Mat>& image_output() {
    return output_image_;
  };

 private:
  DataInput<cv::Mat> input_image_;
  DataOutput<cv::Mat> output_image_;
  //cv::Ptr<cv::cuda::CascadeClassifier> cascade_gpu_;
};

#endif /* OBJECTDETECTOR_H */

