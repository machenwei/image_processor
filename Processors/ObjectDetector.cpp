/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ObjectDetector.cpp
 * Author: machenwei
 * 
 * Created on January 25, 2018, 8:09 PM
 */

#include "ObjectDetector.h"

#include <iostream>

using namespace std;
using namespace cv;

void ObjectDetector::process() {
  {
    const auto& l1 = input_image_.get_lock();
    const auto& l2 = output_image_.get_lock();

    const Mat& orignalImage = input_image_.get();
    //cuda::GpuMat gpu_img(orignalImage);
    
    //cuda::GpuMat objbuf;
    //cascade_gpu_->detectMultiScale(gpu_img, objbuf);
    
    
    //std::vector<Rect> faces;
    //cascade_gpu_->convert(objbuf, faces);
    
    
    //Mat outImage(orignalImage.rows, orignalImage.cols, CV_8UC3);
    //orignalImage.copyTo(outImage);
    
    //for(const auto& face : faces) {
    //   cv::rectangle(outImage, face, Scalar(255));
    //}
    //outImage.copyTo(output_image_.get_mutable());
    orignalImage.copyTo(output_image_.get_mutable());
  }
  output_image_.provide();
}


