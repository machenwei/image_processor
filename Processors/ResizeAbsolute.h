/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ResizeAbsolute.h
 * Author: machenwei
 *
 * Created on January 17, 2018, 8:06 PM
 */

#ifndef RESIZEABSOLUTE_H
#define RESIZEABSOLUTE_H

#include <iostream>

#include "Processor.h"
#include "DataInput.h"
#include "DataOutput.h"

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"

using namespace std;

class ResizeAbsolute : public Processor {
 public:
  ResizeAbsolute(int width, int height) : 
    raw_image_(this),
    resized_image_(this),
    width_(width),
    height_(height) {};

  ResizeAbsolute(const ResizeAbsolute& orig) : 
    raw_image_(this),
    resized_image_(this),
    width_(orig.width_),
    height_(orig.height_) {};
  
  bool ready_to_run() override {
    return raw_image_.has_pending_updates() && 
           resized_image_.is_ready_for_new_value();  
  };  
    
  void process() override;
  
  void initialize() override;

  DataInput<cv::Mat>& image_input() {
    return raw_image_;
  };

  DataOutput<cv::Mat>& image_output() {
    return resized_image_;
  };

 private:
  DataInput<cv::Mat> raw_image_;
  DataOutput<cv::Mat> resized_image_;
  
  int width_;
  int height_;
};

#endif /* RESIZEABSOLUTE_H */

