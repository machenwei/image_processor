/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StereoscopicMerge.cpp
 * Author: machenwei
 * 
 * Created on January 25, 2018, 12:05 AM
 */

#include "StereoscopicMerge.h"

using namespace cv;

void StereoscopicMerge::initialize() {
};

void StereoscopicMerge::process() {
  const auto& l1 = left_image_.get_lock();
  const auto& l2 = right_image_.get_lock();
  const auto& l3 = output_image_.get_lock();
  
  const Mat& l = left_image_.get();
  const Mat& r = right_image_.get();
  
  //assert(l.type() == r.type());
  //assert(l.size() == r.size());
  Mat merged(l.rows, l.cols * 2, l.type());
  Mat lpart = merged.colRange(0, l.cols);
  Mat rpart = merged.colRange(l.cols, merged.cols);
  l.copyTo(lpart);
  r.copyTo(rpart);

  merged.copyTo(output_image_.get_mutable());
  output_image_.provide();
};
  
