/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DisplayWindow.cpp
 * Author: machenwei
 * 
 * Created on January 12, 2018, 10:31 PM
 */

#include "DisplayWindow.h"

#include <stdio.h>
#include <iostream>

#include "Debug.h"

DisplayWindow::DisplayWindow(const std::string& window_name) :
    image_input_(this), // Processor*
    window_name_(window_name) {
}

DisplayWindow::DisplayWindow(const DisplayWindow& orig) : image_input_(this){
}

DisplayWindow::~DisplayWindow() {
}

void DisplayWindow::initialize() {
#ifdef ENABLE_GUI
  cv::namedWindow(window_name_, cv::WINDOW_AUTOSIZE);
#endif
}

void DisplayWindow::process() {
  const auto& l = image_input_.get_lock();
  cv::Mat img = image_input_.get();
  if (!img.empty()) {
    #ifdef ENABLE_GUI
    cv::imshow(window_name_, img);
    #endif
  }
  frame_count_ ++;
}
