#include <stdio.h>
#include <iostream>
#include <memory>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/video/background_segm.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"

#include "CameraSource2.h"
#include "Common.h"
#include "Debug.h"
#include "CustomTransform.h"
#include "DisplayWindow.h"
#include "FileVideoSource.h"
#include "ForegroundExtractor.h"
#include "Gear360FrameSplit.h"
#include "ProcessorExecutor.h"
#include "ProcessorExecutorThread.h"
#include "ResizeAbsolute.h"
#include "StereoscopicMerge.h"

using namespace std;
using namespace cv;

/* Some how the fgbg module keeps running without anything upsteam module
 * invoking it with new data. A cin is put in the filesource module to step\
 * through the frames. A lot of debug prints are put in place.
 * Next Step: 
 * 1. Maybe instrument the modules so that the debug message can be
 * more informative.
 * 2. Figure out WTF is going on with this fgbg module. Why does it fucking get
 * in the run queue all the times.
 * 
 */

static void help() {
  DCOUT("\nDo background segmentation, especially demonstrating the use of cvUpdateBGStatModel().\n"
    "Learns the background at the start and then segments.\n"
    "Learning is togged by the space key. Will read from file or camera\n"
    "Usage: \n"
    "			./bgfg_segm [--camera]=<use camera, if this key is present>, [--file_name]=<path to movie file> \n\n");
}

const char* keys = {
  "{c  camera   |         | use camera or not}"
  "{m  method   |mog2     | method (knn or mog2) }"
  "{s  smooth   |         | smooth the mask }"
  "{fn file_name|../data/tree.avi | movie file        }"
};

#define PI 3.1415926535897932384626433
pair<int, int> FishEyeToEquirectangular(int dest_x, 
                                        int dest_y, 
                                        int source_width, 
                                        int source_height,
                                        int h_fov_degree,
                                        int v_fov_degree) {
  Point2f pfish;
  float theta, phi, r;
  Point3f psph;
  //float FOV = PI;
  float FOV_H = (float) PI / 180 * h_fov_degree;
  float FOV_V = (float) PI / 180 * v_fov_degree;
  ;
  float width = source_width;
  float height = source_height;
  theta = PI * (dest_x / width - 0.5); // -pi to pi
  phi = PI * (dest_y / height - 0.5); // -pi/2 to pi/2
  psph.x = cos(phi) * sin(theta);
  psph.y = cos(phi) * cos(theta);
  psph.z = sin(phi);
  theta = atan2(psph.z, psph.x);
  phi = atan2(sqrt(psph.x * psph.x + psph.z * psph.z), psph.y);
  r = width * phi / FOV_H;
  float r2 = height * phi / FOV_V;
  int source_x = 0.5 * width + r * cos(theta);
  int source_y = 0.5 * height + r2 * sin(theta);
  return pair<int, int>(source_x, source_y);
}

int main(int argc, const char** argv) {
  DCOUT(cv::getBuildInformation());
  CommonInit();
  help();

 
  return 0;
}
